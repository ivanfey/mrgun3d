﻿using Core.Web;
using Newtonsoft.Json;
using PlayFab.ClientModels;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Windows
{
    public class DailyRewardPopUp : AbstractPopUp
    {
        public struct DailyReward
        {
            public DateTime LastRewardTime;
            public int LastRewardNum;
        }

        private const string _dailyRewardParams = "DailyReward";
        public static DailyRewardPopUp Instance = null;
        public UnityEvent OnHided = new UnityEvent();

        [SerializeField]
        private Button _cancelButton = null;
        [SerializeField]
        private DailyRewardUnit[] _dailyRewardUnits = new DailyRewardUnit[0];
        [SerializeField]
        private DailyRewardUnit _weaponReward = null;
        [SerializeField]
        private DailyRewardUnit _noWeaponReward = null;

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public bool CustomShow()
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            if (_showing != null)
                return false;

            var defaultDailyRewardText = JsonConvert.SerializeObject(new DailyReward { LastRewardTime = DateTime.Now, LastRewardNum = -1 });
            if (!PlayerPrefs.HasKey(_dailyRewardParams))
                PlayerPrefs.SetString(_dailyRewardParams, defaultDailyRewardText);

            var lastDailyRewardText = PlayerPrefs.GetString(_dailyRewardParams);
            var lastDailyReward = JsonConvert.DeserializeObject<DailyReward>(lastDailyRewardText);
            if (lastDailyReward.LastRewardTime.AddDays(1) > DateTime.Now)
                return false;
            if(lastDailyReward.LastRewardNum >= 6)
                lastDailyReward.LastRewardNum = -1;

            _showing = Showing(lastDailyReward);
            StartCoroutine(_showing);

            return true;
        }

        private IEnumerator _showing = null;
        private IEnumerator Showing(DailyReward lastDailyReward)
        {
            _cancelButton.interactable = false;

            for (int i = 0; i < _dailyRewardUnits.Length; i++)
            {
                _dailyRewardUnits[i].SetSelected(false);
            }

            var isShowed = false;
            base.Show(()=> 
            {
                isShowed = true;
            });

            while (!isShowed)
                yield return null;

            _weaponReward.gameObject.SetActive(false);
            _noWeaponReward.gameObject.SetActive(true);
            _dailyRewardUnits[_dailyRewardUnits.Length - 1] = _noWeaponReward;
            CatalogItem weaponFromCatalog = null;
            var weaponCatalog = CatalogController.WeaponCatalog;
            var playerInventory = InventoryController.InventoryResult;
            for (int i = 0; i < weaponCatalog.Catalog.Count; i++)
            {
                if (weaponCatalog.Catalog[i].Tags[0] == "Tier_3")
                    continue;

                var isFinded = false;
                for (int j = 0; j < playerInventory.Inventory.Count; j++)
                {
                    if (playerInventory.Inventory[j].ItemId != weaponCatalog.Catalog[i].ItemId)
                        continue;

                    isFinded = true;
                    break;
                }

                if (isFinded)
                    continue;

                weaponFromCatalog = weaponCatalog.Catalog[i];
                _weaponReward.SetRewardName(weaponFromCatalog.DisplayName);
                _weaponReward.gameObject.SetActive(true);
                _noWeaponReward.gameObject.SetActive(false);
                _dailyRewardUnits[_dailyRewardUnits.Length - 1] = _weaponReward;
                break;
            }

            for (int i = 0; i < _dailyRewardUnits.Length; i++)
            {
                if (!_dailyRewardUnits[i].isActiveAndEnabled)
                    continue;

                yield return new WaitForSeconds(0.1f);
                if (i > lastDailyReward.LastRewardNum)
                    break;
                _dailyRewardUnits[i].SetSelected(true);
            }
            yield return new WaitForSeconds(0.25f);
            _dailyRewardUnits[lastDailyReward.LastRewardNum + 1].SetSelected(true);
            switch (_dailyRewardUnits[lastDailyReward.LastRewardNum + 1].GetReward())
            {
                case DailyRewardUnit.RewardType.Money25:
                    InventoryController.AddMoney(25);
                    break;
                case DailyRewardUnit.RewardType.Money50:
                    InventoryController.AddMoney(50);
                    break;
                case DailyRewardUnit.RewardType.Money75:
                    InventoryController.AddMoney(75);
                    break;
                case DailyRewardUnit.RewardType.Money100:
                    InventoryController.AddMoney(100);
                    break;
                case DailyRewardUnit.RewardType.Money150:
                    InventoryController.AddMoney(150);
                    break;
                case DailyRewardUnit.RewardType.Money200:
                    InventoryController.AddMoney(200);
                    break;
                case DailyRewardUnit.RewardType.Money250:
                    InventoryController.AddMoney(250);
                    break;
                case DailyRewardUnit.RewardType.Weapon:
                    var weaponPrice = (int)weaponFromCatalog.VirtualCurrencyPrices["CO"];
                    InventoryController.AddMoney(weaponPrice, ()=> 
                    {
                        InventoryController.PurchaseItem(weaponFromCatalog);
                    });
                    break;
            }
            var newLastDailyRewardText = JsonConvert.SerializeObject(new DailyReward { LastRewardTime = DateTime.Now, LastRewardNum = lastDailyReward.LastRewardNum + 1 });
            PlayerPrefs.SetString(_dailyRewardParams, newLastDailyRewardText);

            _cancelButton.interactable = true;
            _showing = null;
        }

        public void CustomHide()
        {
            base.Hide(()=> OnHided.Invoke());
        }
    }
}

