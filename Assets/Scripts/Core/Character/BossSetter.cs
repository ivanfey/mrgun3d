﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Character
{
    public class BossSetter : MonoBehaviour
    {
        [System.Serializable]
        public class BlackList
        {
            [System.Serializable]
            public class BossType
            {
                public bool IsMale = false;
                public string Name = "";
                public bool IsDie = false;
                public int SkinId = 0;

                public BossType(bool isMale, string name, int skinId)
                {
                    IsMale = isMale;
                    Name = name;
                    SkinId = skinId;
                    IsDie = false;
                }
            }

            public int BlackListNum = 0;
            public List<BossType> Bosses = new List<BossType>();
        }

        public static BossSetter Instance = null;

        private const string _actualBlackListParam = "BlackList";
        private const string _oldBlackListParam = "BlackListOld";

        [SerializeField]
        private string[] _namePrefixes = new string[0];
        [SerializeField]
        private string[] _nameSuffixes = new string[0];
        [SerializeField]
        private string[] _nameMale = new string[0];
        [SerializeField]
        private string[] _nameFemale = new string[0];

        private BlackList _blacklist = null;

        [ContextMenu("Clear list")]
        private void ClearList()
        {
            PlayerPrefs.DeleteKey(_actualBlackListParam);
            PlayerPrefs.DeleteKey(_oldBlackListParam);
        }

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            var blackListText = JsonConvert.SerializeObject(_blacklist);
            PlayerPrefs.SetString(_actualBlackListParam, blackListText);
            Instance = null;
        }

        public int GetActualBlackListNumber()
        {
            if (_blacklist == null)
                return 0;
            return _blacklist.BlackListNum;
        }

        public int GetOldBlackListNumber()
        {
            if (!PlayerPrefs.HasKey(_oldBlackListParam))
                return 0;
            var oldBlackList = JsonConvert.DeserializeObject<BlackList>(PlayerPrefs.GetString(_oldBlackListParam));
            return oldBlackList.BlackListNum;
        }

        public string GetOldBossNames()
        {
            if (!PlayerPrefs.HasKey(_oldBlackListParam))
                return null;
            var oldBlackList = JsonConvert.DeserializeObject<BlackList>(PlayerPrefs.GetString(_oldBlackListParam));
            var bossNames = oldBlackList.Bosses;
            var message = "";
            for (int i = 0; i < bossNames.Count; i++)
            {
                if (bossNames[i].IsDie)
                    message += "<s>";
                message += bossNames[i].Name;
                if (bossNames[i].IsDie)
                    message += "</s>";
                message += "\n\n";
            }

            return message;
        }

        public string GetBossNames()
        {
            var message = "";
            for (int i = 0; i < _blacklist.Bosses.Count; i++)
            {
                if (_blacklist.Bosses[i].IsDie)
                    message += "<s>";
                message += _blacklist.Bosses[i].Name;
                if (_blacklist.Bosses[i].IsDie)
                    message += "</s>";
                message += "\n\n";
            }

            return message;
        }

        public string GetBossName()
        {
            for (int i = 0; i < _blacklist.Bosses.Count; i++)
            {
                if (!_blacklist.Bosses[i].IsDie)
                    return _blacklist.Bosses[i].Name;
            }

            return null;
        }

        public BlackList GetOrRandomize(int count, out bool isDone)
        {
            isDone = false;
            if(PlayerPrefs.HasKey(_actualBlackListParam))
            {
                _blacklist = JsonConvert.DeserializeObject<BlackList>(PlayerPrefs.GetString(_actualBlackListParam));
                if(_blacklist != null && _blacklist.Bosses != null)
                {
                    for (int i = 0; i < _blacklist.Bosses.Count; i++)
                    {
                        if (!_blacklist.Bosses[i].IsDie)
                            return _blacklist;
                    }
                    Debug.Log("Blacklist done!");
                    PlayerPrefs.SetString(_oldBlackListParam, PlayerPrefs.GetString(_actualBlackListParam));
                    isDone = true;
                }
            }

            _blacklist = new BlackList();

            _blacklist.BlackListNum = 1;
            if (PlayerPrefs.HasKey(_oldBlackListParam))
            {
                var oldBlackList = JsonConvert.DeserializeObject<BlackList>(PlayerPrefs.GetString(_oldBlackListParam));
                _blacklist.BlackListNum = oldBlackList.BlackListNum + 1;
            }

            _blacklist.Bosses = new List<BlackList.BossType>();
            var prefixes = new List<string>();
            for (int i = 0; i < _namePrefixes.Length; i++)
                prefixes.Add(_namePrefixes[i]);
            var suffixes = new List<string>();
            for (int i = 0; i < _nameSuffixes.Length; i++)
                suffixes.Add(_nameSuffixes[i]);
            var maleNames = new List<string>();
            for (int i = 0; i < _nameMale.Length; i++)
                maleNames.Add(_nameMale[i]);
            var femaleNames = new List<string>();
            for (int i = 0; i < _nameFemale.Length; i++)
                femaleNames.Add(_nameFemale[i]);

            for (int i = 0; i < count; i++)
            {
                var isMale = Random.Range(-10f, 10f) > 0f;
                var isPrefix = Random.Range(-10f, 10f) > 0f;
                var randomPrefix = prefixes[Random.Range(0, prefixes.Count)];
                prefixes.Remove(randomPrefix);
                var randomSuffix = suffixes[Random.Range(0, suffixes.Count)];
                suffixes.Remove(randomSuffix);
                var randomMaleName = maleNames[Random.Range(0, maleNames.Count)];
                maleNames.Remove(randomMaleName);
                var randomFemaleName = femaleNames[Random.Range(0, femaleNames.Count)];
                femaleNames.Remove(randomFemaleName);

                var firstPart = isPrefix ? randomPrefix : (isMale ? randomMaleName : randomFemaleName);
                var secondPart = isPrefix ? (isMale ? randomMaleName : randomFemaleName) : randomSuffix;
                var name = firstPart + " " + secondPart;
                _blacklist.Bosses.Add(new BlackList.BossType(isMale, name, Random.Range(0, 100)));
            }

            var blackListText = JsonConvert.SerializeObject(_blacklist);
            PlayerPrefs.SetString(_actualBlackListParam, blackListText);
            return _blacklist;
        }
    }
}