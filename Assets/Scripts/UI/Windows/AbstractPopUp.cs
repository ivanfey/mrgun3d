﻿using Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Windows
{
    public abstract class AbstractPopUp : MonoBehaviour
    {
        [SerializeField]
        private Transform _root = null;
        [SerializeField]
        private Image _backgroundImage = null;
        [SerializeField]
        private Color _defaultColor = Color.black;

        [SerializeField]
        protected float _showTime = 0.75f;
        [SerializeField]
        protected AnimationCurve _showCurve = new AnimationCurve();

        [SerializeField]
        protected float _hideTime = 0.75f;
        [SerializeField]
        protected AnimationCurve _hideCurve = new AnimationCurve();

        public virtual void Show(UnityAction onShowed = null)
        {
            if (_moving != null)
                StopCoroutine(_moving);

            _root.gameObject.SetActive(true);
            _backgroundImage.enabled = true;
            _moving = Moving(_showTime, Vector3.one, _defaultColor, _showCurve, () =>
            {
                onShowed?.Invoke();
            });
            StartCoroutine(_moving);
        }

        public virtual void Hide(UnityAction onHided = null)
        {
            if (_moving != null)
                StopCoroutine(_moving);

            _moving = Moving(_hideTime, Vector3.zero, Color.clear, _hideCurve, () =>
            {
                _root.gameObject.SetActive(false);
                _backgroundImage.enabled = false;
                onHided?.Invoke();
            });
            StartCoroutine(_moving);
        }

        private IEnumerator _moving = null;
        private IEnumerator Moving(float moveTime, Vector3 targetScale, Color targetColor, AnimationCurve moveCurve, UnityAction onDoned)
        {
            var startScale = _root.localScale;
            var startColor = _backgroundImage.color;
            var timer = 0f;
            while (timer < moveTime)
            {
                yield return null;
                timer += Time.deltaTime;

                _root.localScale = Vector3.Lerp(startScale, targetScale, moveCurve.Evaluate(timer / moveTime));
                _backgroundImage.color = Color.Lerp(startColor, targetColor, moveCurve.Evaluate(timer / moveTime));
            }

            _moving = null;

            onDoned?.Invoke();
        }
    }
}

