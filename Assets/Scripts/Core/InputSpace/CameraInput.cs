﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.InputSpace
{
    public class CameraInput : MonoBehaviour
    {
        public static CameraInput Instance = null;

        [SerializeField]
        private float _transTime = 15f;
        [SerializeField]
        private AnimationCurve _moveCurve = null;
        [SerializeField]
        private AnimationCurve _rotCurve = null;
        [SerializeField]
        private Transform _cameraTrans = null;
        [SerializeField]
        private float _shakeSpeed = 50f;

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        private void Update()
        {
            if(_shacking != null)
                _cameraTrans.localPosition = Vector3.Lerp(_cameraTrans.localPosition, Vector3.zero, Time.deltaTime * _shakeSpeed);
        }

        public void Shake(float transitionTime, float shakeCount, float shakeForce)
        {
            if (_shacking != null)
                StopCoroutine(_shacking);
            _shacking = Shacking(transitionTime, shakeCount, shakeForce);
            StartCoroutine(_shacking);
        }

        private IEnumerator _shacking = null;
        private IEnumerator Shacking(float transitionTime, float shakeCount, float shakeForce)
        {
            var count = 0;
            while (count < shakeCount)
            {
                var timer = 0f;
                var startPoint = _cameraTrans.localPosition;
                var targetPoint = Random.insideUnitSphere * shakeForce;
                while (timer <= transitionTime)
                {
                    timer += Time.deltaTime;
                    _cameraTrans.localPosition = Vector3.Lerp(startPoint, targetPoint, timer / transitionTime);
                    yield return null;
                }
                count++;
            }

            _shacking = null;
        }

        public void MoveTo(Vector3 pos, Quaternion rot, UnityAction onDone)
        {
            MoveTo(pos, rot, _transTime, onDone);
        }

        public void MoveTo(Vector3 pos, Quaternion rot, float transTime, UnityAction onDone)
        {
            if (_movingTo != null)
                StopCoroutine(_movingTo);
            _movingTo = MovingTo(pos, rot, transTime, onDone);
            StartCoroutine(_movingTo);
        }

        private IEnumerator _movingTo = null;
        private IEnumerator MovingTo(Vector3 pos, Quaternion rot, float transTime, UnityAction onDone)
        {
            var startPos = transform.position;
            var startRot = transform.rotation;

            var timer = 0f;
            while (timer <= transTime)
            {
                yield return null;
                timer += Time.deltaTime;
                transform.position = Vector3.Lerp(startPos, pos, _moveCurve.Evaluate(timer / transTime));
                transform.rotation = Quaternion.Lerp(startRot, rot, _rotCurve.Evaluate(timer / transTime));
            }

            _movingTo = null;
            onDone?.Invoke();
        }
    }
}

