﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using Ads;
using UnityEngine.Advertisements;

namespace UI.Windows
{
    public class AfterDeadMenuWindow : AbstractWindow
    {
        public static UnityEvent OnAdsDone = new UnityEvent();

        [SerializeField]
        private TextMeshProUGUI _timerTextArea = null;
        [SerializeField]
        private float _maxTimer = 9f;
        [SerializeField]
        private Image _backgroundImage = null;
        [SerializeField]
        private Color _defaultColor = Color.black;
        [SerializeField]
        private Button _noThanksButton = null;
        [SerializeField]
        private Button _adsDutton = null;
        [SerializeField]
        private Transform _backgroundTrans = null;
        [SerializeField]
        private float _backgroundOffset = 149.71f;

        private float _timer = 0f;
        private bool _isPause = false;

        private void OnEnable()
        {
            _isPause = false;
            _noThanksButton.onClick.AddListener(EndGame);
            _adsDutton.onClick.AddListener(SecondLife);
            Rewarded.OnRewardedVideoReady.AddListener(CheckAdsButton);
        }

        private void OnDisable()
        {
            _noThanksButton.onClick.RemoveListener(EndGame);
            _adsDutton.onClick.RemoveListener(SecondLife);
            Rewarded.OnRewardedVideoReady.RemoveListener(CheckAdsButton);
        }

        private void CheckAdsButton()
        {
            _adsDutton.interactable = Advertisement.IsReady(Rewarded.PlacementId);
        }

        public override void Show(UnityAction onShowed = null)
        {
            Analytics.AnalyticsController.OpenWindow(gameObject.name);

            base.Show(onShowed);

            _timer = _maxTimer;

            if (_showing != null)
                StopCoroutine(_showing);

            _showing = Showing(_showTime, _defaultColor, _showCurve, () =>
            {
                onShowed?.Invoke();
            });
            StartCoroutine(_showing);
        }

        public override void Hide(UnityAction onHided = null)
        {
            base.Hide(onHided);

            if (_showing != null)
                StopCoroutine(_showing);

            _showing = Showing(_hideTime, Color.clear, _hideCurve, () =>
            {
                onHided?.Invoke();
            });
            StartCoroutine(_showing);
        }

        private IEnumerator _showing = null;
        private IEnumerator Showing(float moveTime, Color endColor, AnimationCurve moveCurve, UnityAction onDoned)
        {
            var startColor = _backgroundImage.color;
            var timer = 0f;
            while (timer < moveTime)
            {
                yield return null;
                timer += Time.deltaTime;

                _backgroundImage.color = Color.Lerp(startColor, endColor, moveCurve.Evaluate(timer / moveTime));
            }

            _showing = null;

            onDoned?.Invoke();
        }

        private void Update()
        {
            if (_showing != null)
                return;

            if (Mathf.FloorToInt(_timer) <= 0)
                return;

            if(!_isPause)
                _timer -= Time.deltaTime;
            var tempTimer = Mathf.FloorToInt(_timer);
            _timerTextArea.text = tempTimer.ToString();

            _backgroundTrans.localEulerAngles = Vector3.forward * _backgroundOffset + (Vector3.forward * 360f * _maxTimer) * _timer / _maxTimer;

            if (tempTimer <= 0)
                EndGame();
        }

        private void EndGame()
        {
            WindowController.Instance.SetNewWindow("AfterAds");
        }

        private void SecondLife()
        {
            Rewarded.ShowRewardedVideo(()=> _isPause = true, ()=> _isPause = false, ()=> 
            {
                OnAdsDone.Invoke();
                Analytics.AnalyticsController.WatchAdsForSecondLife();
            }, ()=> _isPause = false);
        }
    }
}