﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Core.Character
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CharacterStats))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class CharacterDier : MonoBehaviour
    {
        [SerializeField]
        private bool _dieOnStart = false;
        [SerializeField]
        private string _hitParamName = "Hit";

        private Animator _anim = null;
        private CharacterStats _charStats = null;
        private NavMeshAgent _navAgent = null;

        private Rigidbody[] _rigidBodyes = new Rigidbody[0];

        private void Init()
        {
            _rigidBodyes = transform.GetComponentsInChildren<Rigidbody>();
            for (int i = 0; i < _rigidBodyes.Length; i++)
                _rigidBodyes[i].isKinematic = true;

            _anim = GetComponent<Animator>();
            _charStats = GetComponent<CharacterStats>();
            _navAgent = GetComponent<NavMeshAgent>();
        }

        private void OnEnable()
        {
            Init();
            _charStats.OnDamaged.AddListener(Damaged);
            _charStats.OnDie.AddListener(Die);
        }

        private void OnDisable()
        {
            _charStats.OnDamaged.RemoveListener(Damaged);
            _charStats.OnDie.RemoveListener(Die);
        }

        private IEnumerator Start()
        {
            yield return null;
            yield return null;
            if (_dieOnStart)
                Die();
        }

        [ContextMenu("Damaged")]
        public void Damaged()
        {
            if(!_anim.GetCurrentAnimatorStateInfo(0).IsTag("Hit") && !_anim.GetNextAnimatorStateInfo(0).IsTag("Hit"))
                _anim.SetTrigger(_hitParamName);
        }

        [ContextMenu("Die")]
        public void Die()
        {
            var weapon = GetComponentInChildren<Weapons.Weapon>();
            weapon.transform.SetParent(_anim.GetBoneTransform(HumanBodyBones.RightHand));

            for (int i = 0; i < _rigidBodyes.Length; i++)
            {
                _rigidBodyes[i].velocity = Vector3.zero;
                _rigidBodyes[i].angularVelocity = Vector3.zero;
                _rigidBodyes[i].isKinematic = false;
            }

            _anim.enabled = false;
            _navAgent.enabled = false;

            StartCoroutine(DisableRB());
        }

        private IEnumerator DisableRB()
        {
            yield return new WaitForSeconds(5f);
            for (int i = 0; i < _rigidBodyes.Length; i++)
                _rigidBodyes[i].gameObject.SetActive(false);
        }
    }
}