﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace UI.Windows
{
    public class AfterAdsMenuWindow : AbstractWindow
    {
        [SerializeField]
        private Image _backgroundImage = null;
        [SerializeField]
        private Color _defaultColor = Color.black;
        [SerializeField]
        private Button _restartButton = null;

        private void OnEnable()
        {
            _restartButton.onClick.AddListener(RestartGame);
        }

        private void OnDisable()
        {
            _restartButton.onClick.RemoveListener(RestartGame);
        }

        public override void Show(UnityAction onShowed = null)
        {
            Analytics.AnalyticsController.OpenWindow(gameObject.name);

            base.Show(onShowed);

            if (_showing != null)
                StopCoroutine(_showing);

            _showing = Showing(_showTime, _defaultColor, _showCurve, () =>
            {
                onShowed?.Invoke();
            });
            StartCoroutine(_showing);
        }

        public override void Hide(UnityAction onHided = null)
        {
            base.Hide(onHided);

            if (_showing != null)
                StopCoroutine(_showing);

            _showing = Showing(_hideTime, Color.clear, _hideCurve, () =>
            {
                onHided?.Invoke();
            });
            StartCoroutine(_showing);
        }

        private IEnumerator _showing = null;
        private IEnumerator Showing(float moveTime, Color endColor, AnimationCurve moveCurve, UnityAction onDoned)
        {
            var startColor = _backgroundImage.color;
            var timer = 0f;
            while (timer < moveTime)
            {
                yield return null;
                timer += Time.deltaTime;

                _backgroundImage.color = Color.Lerp(startColor, endColor, moveCurve.Evaluate(timer / moveTime));
            }

            _showing = null;

            onDoned?.Invoke();
        }

        private void RestartGame()
        {
            Core.Web.InventoryController.SubtractAllScore();
            SceneManager.LoadScene(0, LoadSceneMode.Single);
        }
    }
}