﻿using UnityEngine;

namespace UI
{
    public class InStoreRotator : MonoBehaviour
    {
        [SerializeField]
        private float _rotateSpeed = 5f;

        private void Update()
        {
            transform.rotation *= Quaternion.Euler(Vector3.up * _rotateSpeed * Time.deltaTime);
        }
    }
}

