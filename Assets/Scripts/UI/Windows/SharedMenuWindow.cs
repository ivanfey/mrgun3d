﻿using Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Windows
{
    public class SharedMenuWindow : MonoBehaviour
    {
        public static SharedMenuWindow Instance = null;

        [SerializeField]
        private Image _levelFiller = null;
        [SerializeField]
        private Transform _levelRoot = null;
        [SerializeField]
        private float _hidingLevelTime = 0.75f;
        [SerializeField]
        private AnimationCurve _hidingCurve = new AnimationCurve();
        [SerializeField]
        private float _showingLevelTime = 0.75f;
        [SerializeField]
        private AnimationCurve _showingCurve = new AnimationCurve();
        [SerializeField]
        private float _fillingSpeed = 15f;

        private void OnEnable()
        {
            Analytics.AnalyticsController.OpenWindow(gameObject.name);

            Instance = this;
            MainController.OnBossSpawned.AddListener(HideLevel);
        }

        private void OnDisable()
        {
            Instance = null;
            MainController.OnBossSpawned.RemoveListener(HideLevel);
        }

        public void HideLevel(GameObject bossGO = null)
        {
            if (_changingLevel != null)
                StopCoroutine(_changingLevel);
            _changingLevel = ChangingLevel(_hidingLevelTime, Vector3.zero, _hidingCurve);
            StartCoroutine(_changingLevel);
        }

        public void ShowLevel()
        {
            if (_changingLevel != null)
                StopCoroutine(_changingLevel);
            _changingLevel = ChangingLevel(_showingLevelTime, Vector3.one, _showingCurve);
            StartCoroutine(_changingLevel);
        }

        private IEnumerator _changingLevel = null;
        private IEnumerator ChangingLevel(float changeTime, Vector3 targetSize, AnimationCurve changeCurve)
        {
            var startSize = _levelRoot.localScale;
            var timer = 0f;
            while (timer < changeTime)
            {
                yield return null;

                timer += Time.deltaTime;

                _levelRoot.localScale = Vector3.Lerp(startSize, targetSize, changeCurve.Evaluate(timer / changeTime));
            }

            _changingLevel = null;
        }

        private void Update()
        {
            _levelFiller.fillAmount = Mathf.Lerp(_levelFiller.fillAmount, MainController.GetInhLevel(), Time.deltaTime * _fillingSpeed);
        }
    }
}