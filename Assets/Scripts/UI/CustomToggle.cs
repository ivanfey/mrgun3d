﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI
{
    public class CustomToggle : MonoBehaviour
    {
        [System.Serializable]
        public class OnCustomTriggered : UnityEvent<bool> { }

        public OnCustomTriggered OnCustomTriggeredEvent = new OnCustomTriggered();

        [SerializeField]
        private Button _onButton = null;
        [SerializeField]
        private Button _offButton = null;

        private void OnEnable()
        {
            _onButton.onClick.RemoveAllListeners();
            _onButton.onClick.AddListener(Off);

            _offButton.onClick.RemoveAllListeners();
            _offButton.onClick.AddListener(On);
        }

        public void SetToggle(bool state)
        {
            if (state)
                On();
            else
                Off();
        }

        private void On()
        {
            _onButton.gameObject.SetActive(true);
            _offButton.gameObject.SetActive(false);

            OnCustomTriggeredEvent.Invoke(true);
        }

        private void Off()
        {
            _onButton.gameObject.SetActive(false);
            _offButton.gameObject.SetActive(true);

            OnCustomTriggeredEvent.Invoke(false);
        }
    }
}