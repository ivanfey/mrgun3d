﻿using Core.Character.Weapons;
using Core.InputSpace;
using Effects;
using System.Collections;
using UnityEngine;

namespace Core.Character
{
    public interface IAttackable
    {
        void Attacking(Animator anim, CharacterAttacker attacker, bool isAttack);
        void Hit(Animator anim, GameObject damager);
    }

    [System.Serializable]
    public class Shoot : IAttackable
    {
        [SerializeField]
        private float _shootRate = 1.5f;
        
        [SerializeField]
        private Vector3 _backForcePos = Vector3.zero;
        [SerializeField]
        private Vector3 _backForceRot = Vector3.zero;
        [SerializeField]
        private float _backForceSpeed = 7.5f;
        [SerializeField]
        private Transform[] _shootPoints = new Transform[0];
        
        [SerializeField]
        private float _startingBulletSpeed = 75f;
        [SerializeField]
        private float _startingBulletGravity = 0.01f;
        [SerializeField]
        private float _inpactImpulseRadius = 0.75f;

        [SerializeField]
        private Transform _weaponBody = null;
        [SerializeField]
        private Transform _rightHandPoint = null;
        [SerializeField]
        private Transform _leftHandPoint = null;

        [SerializeField]
        private string _bulletName = "SimpleBullet";
        [SerializeField]
        private string _shootEffectName = "Smoke";
        [SerializeField]
        private AudioSource _audioSource = null;

        private float _damage = 1f;
        private int _shootCount = 1;
        private float _shootRateInSpray = 0.1f;

        private Vector3 _startLocalPosition = Vector3.zero;
        private Vector3 _startLocalEulerAngle = Vector3.zero;
        
        private float _oldShootTime = 0f;

        public void SetDamage(float damage)
        {
            _damage = damage;
        }

        public void SetShootCount(int shootCount)
        {
            _shootCount = shootCount;
        }

        public void SetShootRate(float shootRate)
        {
            _shootRateInSpray = shootRate;
        }

        public void SetStartingParams(Vector3 startLocalPosition, Vector3 startLocalEulerAngle)
        {
            _startLocalPosition = startLocalPosition;
            _startLocalEulerAngle = startLocalEulerAngle;

            _oldShootTime = Time.time;
        }

        public void Attacking(Animator anim, CharacterAttacker attacker, bool isAttack)
        {
            if (_weaponBody == null)
                return;

            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKPosition(AvatarIKGoal.RightHand, _rightHandPoint.position);
            anim.SetIKPosition(AvatarIKGoal.LeftHand, _leftHandPoint.position);

            anim.SetIKRotationWeight(AvatarIKGoal.RightHand, 1f);
            anim.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1f);
            anim.SetIKRotation(AvatarIKGoal.RightHand, _rightHandPoint.rotation);
            anim.SetIKRotation(AvatarIKGoal.LeftHand, _leftHandPoint.rotation);

            var backForceSpeed = Time.deltaTime * _backForceSpeed;
            _weaponBody.localPosition = Vector3.Lerp(_weaponBody.localPosition, _startLocalPosition, backForceSpeed);
            _weaponBody.localRotation = Quaternion.Lerp(_weaponBody.localRotation, Quaternion.Euler(_startLocalEulerAngle), backForceSpeed);

            if (!isAttack)
                return;

            if (_shootRate > Time.time - _oldShootTime)
                return;

            if (_shooting != null)
                return;

            _oldShootTime = Time.time;

            _shooting = Shooting((attacker));
            attacker.StartCoroutine(_shooting);
        }

        public void Hit(Animator anim, GameObject damager)
        {
        }

        private IEnumerator _shooting = null;
        private IEnumerator Shooting(CharacterAttacker attacker)
        {
            var actualShoot = 0;
            while (actualShoot < _shootCount)
            {
                CameraInput.Instance.Shake(_shootRateInSpray, 1, 0.5f);

                for (int i = 0; i < _shootPoints.Length; i++)
                {
                    EffectSpawner.SpawnEffect(_shootPoints[i].position, _shootPoints[i].forward, _shootEffectName);
                    Bullet.SpawnBullet(_shootPoints[i].position, _shootPoints[i].rotation, _damage, _startingBulletSpeed, _startingBulletGravity, _inpactImpulseRadius, attacker, _bulletName);
                }

                _weaponBody.position += _weaponBody.TransformDirection(_backForcePos);
                _weaponBody.localRotation *= Quaternion.Euler(_backForceRot);
                _audioSource.PlayOneShot(_audioSource.clip);
                actualShoot++;

                yield return new WaitForSeconds(_shootRateInSpray);
            }

            yield return null;

            _shooting = null;
        }
    }

    [System.Serializable]
    public class EmptyWeapon : IAttackable
    {
        public void Attacking(Animator anim, CharacterAttacker attacker, bool isAttack)
        {
            anim.SetIKPositionWeight(AvatarIKGoal.RightHand, 0f);
            anim.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0f);
        }

        public void Hit(Animator anim, GameObject damager)
        {
        }
    }

    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(CharacterMover))]
    public class CharacterAttacker : MonoBehaviour
    {
        [SerializeField]
        private EmptyWeapon _emptyWeapon = null;

        private Animator _anim = null;

        private IAttackable _weapon = null;

        private bool _isAttack = false;

        private void Start()
        {
            _anim = GetComponent<Animator>();
            SetEmptyWeapon();
        }

        public void SetInputs(bool isAttack)
        {
            _isAttack = isAttack;
        }

        private void OnAnimatorIK()
        {
            if (_weapon == null)
                return;

            _weapon.Attacking(_anim, this, _isAttack);
        }

        private void SendEvent()
        {
            _weapon.Hit(_anim, gameObject);
        }

        public void SetNewWeapon(IAttackable weapon)
        {
            _weapon = weapon;
        }

        public void SetEmptyWeapon()
        {
            SetNewWeapon(_emptyWeapon);
        }
    }
}