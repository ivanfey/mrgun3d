﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class SomeScoreShower : MonoBehaviour
    {
        private enum ScoreType
        {
            Score,
            ScoreMax,
            Money,
            MoneyMax,
            HeadShots,
            HeadShotsMax,
            Level
        }

        [SerializeField]
        private ScoreType _scoreType = ScoreType.Score;
        [SerializeField]
        private string _addText = "";

        private TextMeshProUGUI _textArea = null;

        private void Awake()
        {
            _textArea = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable()
        {
            switch (_scoreType)
            {
                case ScoreType.Score:
                    ShowInfo(Core.Web.InventoryController.Score);
                    Core.Web.InventoryController.OnScoreChanged.AddListener(ShowInfo);
                    break;
                case ScoreType.ScoreMax:
                    ShowInfo(Core.Web.InventoryController.ScoreMax);
                    Core.Web.InventoryController.OnScoreMaxChanged.AddListener(ShowInfo);
                    break;
                case ScoreType.HeadShots:
                    ShowInfo(Core.Web.InventoryController.HeadShots);
                    Core.Web.InventoryController.OnHeadShotChanged.AddListener(ShowInfo);
                    break;
                case ScoreType.HeadShotsMax:
                    ShowInfo(Core.Web.InventoryController.HeadShotsCollected);
                    Core.Web.InventoryController.OnHeadShotCollectedChanged.AddListener(ShowInfo);
                    break;
                case ScoreType.Money:
                    ShowInfo(Core.Web.InventoryController.Money);
                    Core.Web.InventoryController.OnMoneyChanged.AddListener(ShowInfo);
                    break;
                case ScoreType.MoneyMax:
                    ShowInfo(Core.Web.InventoryController.MoneyCollected);
                    Core.Web.InventoryController.OnMoneyCollectedChanged.AddListener(ShowInfo);
                    break;
                case ScoreType.Level:
                    ShowInfo(Core.Web.InventoryController.Level);
                    Core.Web.InventoryController.OnLevelChanged.AddListener(ShowInfo);
                    break;
            }
        }

        private void OnDisable()
        {
            switch (_scoreType)
            {
                case ScoreType.Score:
                    Core.Web.InventoryController.OnScoreChanged.RemoveListener(ShowInfo);
                    break;
                case ScoreType.ScoreMax:
                    Core.Web.InventoryController.OnScoreMaxChanged.RemoveListener(ShowInfo);
                    break;
                case ScoreType.HeadShots:
                    Core.Web.InventoryController.OnHeadShotChanged.RemoveListener(ShowInfo);
                    break;
                case ScoreType.HeadShotsMax:
                    Core.Web.InventoryController.OnHeadShotCollectedChanged.RemoveListener(ShowInfo);
                    break;
                case ScoreType.Money:
                    Core.Web.InventoryController.OnMoneyChanged.RemoveListener(ShowInfo);
                    break;
                case ScoreType.MoneyMax:
                    Core.Web.InventoryController.OnMoneyCollectedChanged.RemoveListener(ShowInfo);
                    break;
                case ScoreType.Level:
                    Core.Web.InventoryController.OnLevelChanged.RemoveListener(ShowInfo);
                    break;
            }
        }

        private void ShowInfo(int value)
        {
            _textArea.text = _addText + value;
        }
    }
}