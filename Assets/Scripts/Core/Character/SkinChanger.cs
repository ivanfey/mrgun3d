﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Character
{
    public class SkinChanger : MonoBehaviour
    {
        public static SkinChanger Instance = null;

        [SerializeField]
        private GameObject[] _skins = new GameObject[0];

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void SetNewSkin(string skinId)
        {
            for (int i = 0; i < _skins.Length; i++)
            {
                _skins[i].SetActive(skinId == _skins[i].name);
            }
        }

        public string GetActualSkinId()
        {
            for (int i = 0; i < _skins.Length; i++)
            {
                if (_skins[i].activeSelf)
                    return _skins[i].name;
            }
            return null;
        }
    }
}

