﻿using PlayFab;
using PlayFab.ServerModels;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Web
{
    public class LeaderboardController
    {
        public static void GetLeaderboardAroundUser(string statName, Action<GetLeaderboardAroundUserResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var leaderboardRequest = new GetLeaderboardAroundUserRequest { PlayFabId = LoginController.LoginRes.PlayFabId, StatisticName = statName, MaxResultsCount = 1 };
            PlayFabServerAPI.GetLeaderboardAroundUser(leaderboardRequest, (result) =>
            {
                Debug.Log("Leaderboard around user getting done");
                OnSuccess?.Invoke(result);
            }, (error) =>
            {
                Debug.LogError("Leaderboard around user getting failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }

        public static void GetLeaderboard(string statName, Action<GetLeaderboardResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var leaderboardRequest = new GetLeaderboardRequest { StatisticName = statName, StartPosition = 0, MaxResultsCount = 10 };
            PlayFabServerAPI.GetLeaderboard(leaderboardRequest, (result) =>
            {
                Debug.Log("Leaderboard getting done");
                OnSuccess?.Invoke(result);
            }, (error) =>
            {
                Debug.LogError("Leaderboard getting failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }

        public static void AddInLeaderboard(string statName, int value)
        {
            var addInLeaderboardRequest = new UpdatePlayerStatisticsRequest { PlayFabId = LoginController.LoginRes.PlayFabId, Statistics = new List<StatisticUpdate> { new StatisticUpdate { StatisticName = statName, Value = value } } };
            PlayFabServerAPI.UpdatePlayerStatistics(addInLeaderboardRequest, (result) =>
            {
                Debug.Log("Update " + statName + " done");
            }, (error) =>
            {
                Debug.LogError("Updating stats was failed...");
                MainController.OnConnectionFailed.Invoke();
            });
        }
    }
}


