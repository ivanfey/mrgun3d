﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using Ads;
using UnityEngine.Advertisements;
using TMPro;

namespace UI.Windows
{
    public class ShopMenuWindow : AbstractWindow
    {
        [System.Serializable]
        public class ShopSegment
        {
            public enum UnitType
            {
                Weapon,
                Outfit
            }

            public static ShopSegment SelectedSegment = null;

            [SerializeField]
            private string _name = "";
            [SerializeField]
            private string _lastSelectedSave = "";
            [SerializeField]
            private Button _selectButton = null;
            [SerializeField]
            private float _selectedButtonSize = 1.25f;
            [SerializeField]
            private float _defaultButtonSize = 1f;
            [SerializeField]
            private GameObject _segmentRoot = null;
            [SerializeField]
            private GameObject _defaultShopUnitGO = null;
            [SerializeField]
            private UnitType _unitType = UnitType.Weapon;
            [SerializeField]
            private ScrollRect _scrollRect = null;
            [SerializeField]
            private float _scrollrectSens = 5f;
            [SerializeField]
            private Image _tierActivateImage_0 = null;
            [SerializeField]
            private Image _tierActivateImage_1 = null;
            [SerializeField]
            private Image _tierActivateImage_2 = null;

            private ShopMenuWindow _shopMenuWindow = null;
            private List<ShopUnit> _allUnits = new List<ShopUnit>();
            private ShopUnit _actualUnit = null;

            public List<ShopUnit> GetDisabledUnits(int price)
            {
                var units = new List<ShopUnit>();
                for (int i = 0; i < _allUnits.Count; i++)
                {
                    if (_allUnits[i].GetUnitState() == ShopUnit.UnitState.Disabled && _allUnits[i].GetCatalogItem().VirtualCurrencyPrices["CO"] == price)
                        units.Add(_allUnits[i]);
                }
                return units;
            }

            public string GetLastSelectedSave()
            {
                return _lastSelectedSave;
            }

            public int GetUnitArrayId(ShopUnit unit)
            {
                for (int i = 0; i < _allUnits.Count; i++)
                {
                    if (_allUnits[i] != unit)
                        continue;
                    return i;
                }
                return 0;
            }

            public void Init(ShopMenuWindow shopMenuWindow)
            {
                _shopMenuWindow = shopMenuWindow;

                for (int i = 0; i < _allUnits.Count; i++)
                {
                    Destroy(_allUnits[i].gameObject);
                }
                _allUnits.Clear();
                var catalogList = new List<PlayFab.ClientModels.CatalogItem>();
                switch (_unitType)
                {
                    case UnitType.Weapon:
                        catalogList = Core.Web.CatalogController.WeaponCatalog.Catalog;
                        break;
                    case UnitType.Outfit:
                        catalogList = Core.Web.CatalogController.SkinCatalog.Catalog;
                        break;
                }
                for (int i = 0; i < catalogList.Count; i++)
                {
                    if (catalogList[i].Tags[0] == "Tier_3")
                        continue;

                    var unitGO = Instantiate<GameObject>(_defaultShopUnitGO, _defaultShopUnitGO.transform.parent);
                    var unit = unitGO.GetComponent<ShopUnit>();

                    var state = ShopUnit.UnitState.Disabled;
                    for (int j = 0; j < Core.Web.InventoryController.InventoryResult.Inventory.Count; j++)
                    {
                        if (Core.Web.InventoryController.InventoryResult.Inventory[j].ItemId != catalogList[i].ItemId)
                            continue;

                        state = ShopUnit.UnitState.Enabled;
                        break;
                    }

                    unit.Init(_shopMenuWindow, this, catalogList[i], state, _unitType);
                    unitGO.SetActive(true);

                    _allUnits.Add(unit);
                }

                _selectButton.onClick.RemoveAllListeners();
                _selectButton.onClick.AddListener(Select);

                _allUnits[PlayerPrefs.GetInt(_lastSelectedSave, 0)].Select();
            }

            public void Select()
            {
                //only for reduce warnings
                var tempName = _name;
                _name = tempName;


                if (SelectedSegment != null)
                    SelectedSegment.Unselect();
                SelectedSegment = this;
                
                _segmentRoot.SetActive(true);
                _selectButton.transform.localScale = Vector3.one * _selectedButtonSize;
            }

            public void Unselect()
            {
                _segmentRoot.SetActive(false);
                _selectButton.transform.localScale = Vector3.one * _defaultButtonSize;
            }

            public void SetActualUnit(ShopUnit value)
            {
                _actualUnit = value;
            }

            public ShopUnit GetActualUnit()
            {
                return _actualUnit;
            }

            public int Check()
            {
                var price = 250;
                if (_scrollRect == null)
                    return price;

                _tierActivateImage_0.enabled = true;
                _tierActivateImage_1.enabled = false;
                _tierActivateImage_2.enabled = false;
                var targetPosition = new Vector2(0f, 0f);
                if (_scrollRect.normalizedPosition.x > 0.25f && _scrollRect.normalizedPosition.x <= 0.75f)
                {
                    _tierActivateImage_0.enabled = false;
                    _tierActivateImage_1.enabled = true;
                    _tierActivateImage_2.enabled = false;
                    targetPosition = new Vector2(0.5f, 0f);
                    price = 500;
                }
                else if (_scrollRect.normalizedPosition.x > 0.75f)
                {
                    _tierActivateImage_0.enabled = false;
                    _tierActivateImage_1.enabled = false;
                    _tierActivateImage_2.enabled = true;
                    targetPosition = new Vector2(1f, 0f);
                    price = 750;
                }

                if (!Input.GetMouseButton(0))
                    _scrollRect.normalizedPosition = Vector2.Lerp(_scrollRect.normalizedPosition, targetPosition, _scrollrectSens * Time.deltaTime);

                return price;
            }
        }

        [SerializeField]
        private ShopSegment[] _shopSegments = new ShopSegment[0];
        [SerializeField]
        private Button _returnButton = null;
        [SerializeField]
        private Button _adsButton = null;
        [SerializeField]
        private Button _randomBuyButton = null;
        [SerializeField]
        private TextMeshProUGUI _priceValueTextArea = null;
        [SerializeField]
        private Transform _defaultCamPoint = null;
        [SerializeField]
        private Transform _shopCamPoint = null;

        private void Awake()
        {
            ShopSegment.SelectedSegment = null;
        }

        private void OnEnable()
        {
            CheckAdsButton();
            Rewarded.OnRewardedVideoReady.AddListener(CheckAdsButton);
        }

        private void OnDisable()
        {
            Rewarded.OnRewardedVideoReady.RemoveListener(CheckAdsButton);
        }

        private void Update()
        {
            var price = ShopSegment.SelectedSegment.Check();
            _randomBuyButton.interactable = Core.Web.InventoryController.Money >= price && ShopSegment.SelectedSegment.GetDisabledUnits(price).Count > 0 && _randomBuing == null;
            _priceValueTextArea.text = price.ToString();
        }

        private void CheckAdsButton()
        {
            _adsButton.interactable = Advertisement.IsReady(Rewarded.PlacementId);
        }

        public void AddMoney()
        {
            Rewarded.ShowRewardedVideo(null, null, ()=> 
            {
                Core.Web.InventoryController.AddMoney(35);
                Analytics.AnalyticsController.WatchAdsForMoney();
            }, null);
        }

        public void RandomBuy()
        {
            if (_randomBuing != null)
                return;
            _randomBuing = RandomBuing();
            StartCoroutine(_randomBuing);
        }

        private IEnumerator _randomBuing = null;
        private IEnumerator RandomBuing()
        {
            var price = ShopSegment.SelectedSegment.Check();
            var startDisabledUnits = ShopSegment.SelectedSegment.GetDisabledUnits(price);

            var selectCount = 7;
            var speed = 0.075f;
            ShopUnit tempRandomUnit = null;
            var selectedUnitNumber = Random.Range(0, startDisabledUnits.Count);
            for (int i = 0; i <= selectCount; i++)
            {
                tempRandomUnit?.GetBuingSelector().SetActive(false);
                tempRandomUnit = startDisabledUnits[selectedUnitNumber];
                tempRandomUnit.GetBuingSelector().SetActive(true);
                yield return new WaitForSeconds(speed);
                tempRandomUnit.GetBuingSelector().SetActive(false);
                speed += 0.075f;

                selectedUnitNumber++;
                if (selectedUnitNumber >= startDisabledUnits.Count)
                    selectedUnitNumber = 0;

                if (startDisabledUnits.Count <= 1)
                    break;
            }

            tempRandomUnit.GetBuingSelector().SetActive(true);
            yield return new WaitForSeconds(speed);
            tempRandomUnit.Buy();

            _randomBuing = null;
        }

        public override void Show(UnityAction onShowed = null)
        {
            Analytics.AnalyticsController.OpenWindow(gameObject.name);

            base.Show(onShowed);

            _returnButton.onClick.AddListener(BackToMainMenu);
            _randomBuyButton.onClick.AddListener(RandomBuy);
            _adsButton.onClick.AddListener(AddMoney);

            for (int i = 0; i < _shopSegments.Length; i++)
            {
                _shopSegments[i].Init(this);
            }

            _shopSegments[0].Select();

            Core.InputSpace.CameraInput.Instance.MoveTo(_shopCamPoint.position, _shopCamPoint.rotation, _showTime, null);

            SharedMenuWindow.Instance.HideLevel();
        }

        public override void Hide(UnityAction onHided = null)
        {
            base.Hide(onHided);

            _returnButton.onClick.RemoveListener(BackToMainMenu);
            _randomBuyButton.onClick.RemoveListener(RandomBuy);
            _adsButton.onClick.RemoveListener(AddMoney);

            Core.InputSpace.CameraInput.Instance.MoveTo(_defaultCamPoint.position, _defaultCamPoint.rotation, _hideTime, null);

            SharedMenuWindow.Instance.ShowLevel();
        }

        private void BackToMainMenu()
        {
            WindowController.Instance.SetNewWindow("Main");
        }
    }
}