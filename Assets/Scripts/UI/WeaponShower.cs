﻿using UnityEngine;

namespace UI
{
    public class WeaponShower : MonoBehaviour
    {
        public static WeaponShower Instance = null;

        [SerializeField]
        private GameObject[] _weaponsInStore = new GameObject[0];

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void ShowWeapon(string weaponId)
        {
            for (int i = 0; i < _weaponsInStore.Length; i++)
            {
                _weaponsInStore[i].SetActive(weaponId + "_Store" == _weaponsInStore[i].name);
            }
        }
    }
}

