﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI.Windows
{
    public class WindowController : MonoBehaviour
    {
        public static WindowController Instance = null;

        [SerializeField]
        private AbstractWindow[] _menus = new AbstractWindow[0];

        private AbstractWindow _actualWindow = null;

        [ContextMenu("Show main menu")]
        private void SetMainMenu()
        {
            SetNewWindow("Main");
        }

        private void OnEnable()
        {
            Instance = this;

        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void SetNewWindow(string windowName)
        {
            for (int i = 0; i < _menus.Length; i++)
            {
                if (!_menus[i].Check(windowName))
                    continue;

                if (_actualWindow != null)
                {
                    _actualWindow.Hide(() =>
                    {
                        _actualWindow = _menus[i];
                        _actualWindow.Show();
                    });
                }
                else
                {
                    _actualWindow = _menus[i];
                    _actualWindow.Show();
                }

                break;
            }
        }
    }
}