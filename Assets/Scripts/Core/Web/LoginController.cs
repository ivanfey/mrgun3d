﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Web
{
    public class LoginController
    {
        public static UnityEvent OnNameChanged = new UnityEvent();
        public static UnityEvent OnLoginDone = new UnityEvent();

        public static LoginResult LoginRes = null;
        public static string UserName = null;

        public static void Login(Action<LoginResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var loginRequest = new LoginWithCustomIDRequest { CreateAccount = true, CustomId = SystemInfo.deviceUniqueIdentifier };
            PlayFabClientAPI.LoginWithCustomID(loginRequest, (result) =>
            {
                Debug.Log("Registration done");
                LoginRes = result;

                if(LoginRes.NewlyCreated)
                {
                    var userInfoRequest = new UpdateUserTitleDisplayNameRequest { DisplayName = SystemInfo.deviceName };
                    PlayFabClientAPI.UpdateUserTitleDisplayName(userInfoRequest, (userInfo) =>
                    {
                        Debug.Log("Set user name done");
                        UserName = userInfo.DisplayName;

                        OnSuccess?.Invoke(result);
                        OnNameChanged.Invoke();
                        OnLoginDone.Invoke();
                    }, (error) =>
                    {
                        Debug.LogError("Set user name failed");
                        MainController.OnConnectionFailed.Invoke();
                        OnFailed?.Invoke(error);
                    });
                }
                else
                {
                    var userInfoRequest = new GetAccountInfoRequest { };
                    PlayFabClientAPI.GetAccountInfo(userInfoRequest, (userInfo) =>
                    {
                        Debug.Log("Get user name done");
                        UserName = userInfo.AccountInfo.TitleInfo.DisplayName;

                        OnSuccess?.Invoke(result);
                        OnNameChanged.Invoke();
                        OnLoginDone.Invoke();
                    }, (error) =>
                    {
                        Debug.LogError("Get user name failed");
                        MainController.OnConnectionFailed.Invoke();
                        OnFailed?.Invoke(error);
                    });
                }
            }, (error) =>
            {
                Debug.LogError("Registration failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }

        public static void ChangeName(string newName, Action<UpdateUserTitleDisplayNameResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var userInfoRequest = new UpdateUserTitleDisplayNameRequest { DisplayName = newName };
            PlayFabClientAPI.UpdateUserTitleDisplayName(userInfoRequest, (userInfo) =>
            {
                Debug.Log("Set user name done");
                UserName = userInfo.DisplayName;

                OnSuccess?.Invoke(userInfo);
                OnNameChanged.Invoke();
            }, (error) =>
            {
                Debug.LogError("Set user name failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }
    }
}