﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Character.Weapons
{
    public class Bullet : MonoBehaviour
    {
        private const string _bulletPath = "Weapons/Bullets/";
        private const float _bulletLifeTime = 5f;

        public static Dictionary<string, List<Bullet>> BulletsPool = new Dictionary<string, List<Bullet>>();
        public static List<GameObject> Impulses = new List<GameObject>();

        public static UnityEvent OnEnemyHead = new UnityEvent();
        public static UnityEvent OnEnemyBody = new UnityEvent();
        
        [SerializeField]
        private LayerMask _damageableLayers = Physics.AllLayers;

        [SerializeField]
        private Vector3 _damageHalfExtends = Vector3.zero;
        [SerializeField]
        private TrailRenderer _trailRenderer = null;

        private CharacterAttacker _attacker = null;
        private string _bulletName = "";
        private float _damage = 0f;
        private float _startingSpeed = 0f;
        private float _startingGravity = 0f;
        private float _impulseRadius = 0f;

        private float _shootTime = 0f;
        
        private Vector3 _newPos = Vector3.zero;
        private float _distance = 0f;
        private Ray _ray = new Ray();
        private RaycastHit _hit = new RaycastHit();

        public static void SpawnBullet(Vector3 pos, Quaternion rot, float damage, float startingSpeed, float startingGravity, float impulseRadius, CharacterAttacker attacker, string bulletName = "SimpleBullet")
        {
            Bullet tempBullet = null;

            if (!BulletsPool.ContainsKey(bulletName))
                BulletsPool.Add(bulletName, new List<Bullet>());

            if (BulletsPool[bulletName].Count > 0)
            {
                tempBullet = BulletsPool[bulletName][0];
                BulletsPool[bulletName].RemoveAt(0);
            }
            else
            {
                var tempBulletPref = Resources.Load<GameObject>(_bulletPath + bulletName);
                var tempBulletGO = Instantiate(tempBulletPref);
                tempBullet = tempBulletGO.GetComponent<Bullet>();
            }

            tempBullet.Init(pos, rot, damage, startingSpeed, startingGravity, impulseRadius, attacker, bulletName);
        }

        public void Init(Vector3 pos, Quaternion rot, float damage, float startingSpeed, float startingGravity, float impulseRadius, CharacterAttacker attacker, string bulletName)
        {
            _attacker = attacker;
            transform.position = pos;
            transform.rotation = rot;
            _trailRenderer.Clear();
            _damage = damage;
            _startingSpeed = startingSpeed;
            _startingGravity = startingGravity;
            _impulseRadius = impulseRadius;
            _bulletName = bulletName;

            _shootTime = Time.time;
            _newPos = transform.position + (transform.forward * _startingSpeed + Vector3.down * _startingGravity) * Time.deltaTime;
            _distance = Vector3.Distance(transform.position, _newPos);

            gameObject.SetActive(true);
        }

        private void LateUpdate()
        {
            _ray.origin = transform.position;
            _ray.direction = transform.forward;

            if (Physics.BoxCast(_ray.origin, _damageHalfExtends, _ray.direction, out _hit, transform.rotation, _distance, _damageableLayers, QueryTriggerInteraction.Ignore))
            {
                var damageable = _hit.transform.GetComponentInParent<IDamageable>();
                if (damageable != null)
                {
                    var isHead = _hit.transform.CompareTag("Head");
                    if (!_hit.transform.root.CompareTag("Player"))
                    {
                        if (isHead)
                            OnEnemyHead.Invoke();
                        else
                            OnEnemyBody.Invoke();
                    }

                    var tempDamage = isHead ? _damage * 2f : _damage;
                    damageable.Damage(tempDamage, gameObject, _hit);

                    GameObject tempImpulse = null;
                    if (Impulses.Count > 0)
                    {
                        tempImpulse = Impulses[0];
                        tempImpulse.SetActive(true);
                        Impulses.RemoveAt(0);
                    }
                    else
                    {
                        tempImpulse = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        tempImpulse.GetComponent<MeshRenderer>().enabled = false;
                    }
                    tempImpulse.transform.position = _hit.point;
                    tempImpulse.transform.localScale = Vector3.one * _impulseRadius;
                    _attacker.StartCoroutine(DisableImpulseAfterFrame(tempImpulse));
                }

                gameObject.SetActive(false);
                BulletsPool[_bulletName].Add(this);
            }
            else
            {
                transform.LookAt(_newPos);
                transform.position = _newPos;
                _newPos = transform.position + transform.forward * _distance + Vector3.down * _startingGravity * Time.deltaTime;
                _distance = Vector3.Distance(transform.position, _newPos);
            }

            if (_bulletLifeTime < Time.time - _shootTime)
            {
                gameObject.SetActive(false);
                BulletsPool[_bulletName].Add(this);
            }
        }
        
        private IEnumerator DisableImpulseAfterFrame(GameObject disabledGO)
        {
            yield return null;
            yield return null;
            disabledGO.SetActive(false);
            Impulses.Add(disabledGO);
        }
    }
}