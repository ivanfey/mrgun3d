﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UI
{
    public class OutfitShower : MonoBehaviour
    {
        public static OutfitShower Instance = null;

        [SerializeField]
        private GameObject[] _outfitsInStore = new GameObject[0];

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void ShowOutfit(string outfitId)
        {
            for (int i = 0; i < _outfitsInStore.Length; i++)
            {
                _outfitsInStore[i].SetActive(outfitId == _outfitsInStore[i].name);
            }
        }
    }
}