﻿using Core;
using Core.Character;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Windows
{
    public class BattleMenuWindow : AbstractWindow
    {
        [SerializeField]
        private float _showBossHealthTime = 0.75f;
        [SerializeField]
        private AnimationCurve _showingCurve = new AnimationCurve();
        [SerializeField]
        private Transform _bossAreaRoot = null;
        [SerializeField]
        private TextMeshProUGUI _bossNameTextArea = null;
        [SerializeField]
        private Image _bossHealthFiller = null;
        [SerializeField]
        private TextMeshProUGUI _headShootTextArea = null;
        [SerializeField]
        private float _headShotShowTime = 1f;

        private CharacterStats _bossStats = null;

        private void OnEnable()
        {
            MainController.OnBossSpawned.AddListener(ShowBossHealth);
            MainController.OnHeadInpact.AddListener(ShowHeadShoot);
        }

        private void OnDisable()
        {
            MainController.OnBossSpawned.RemoveListener(ShowBossHealth);
            MainController.OnHeadInpact.RemoveListener(ShowHeadShoot);
        }

        public override void Show(UnityAction onShowed = null)
        {
            Analytics.AnalyticsController.OpenWindow(gameObject.name);

            base.Show(onShowed);
        }

        private void Update()
        {
            if (_bossStats != null)
                _bossHealthFiller.fillAmount = _bossStats.MainStats.Health / _bossStats.MainStats.MaxHealth;
        }

        private void ShowBossHealth(GameObject bossGO)
        {
            _bossStats = bossGO.GetComponent<CharacterStats>();
            _bossNameTextArea.text = BossSetter.Instance.GetBossName();

            if (_showingBossHealth != null)
                StopCoroutine(_showingBossHealth);
            _showingBossHealth = ShowingBossHealth();
            StartCoroutine(_showingBossHealth);
        }

        private IEnumerator _showingBossHealth = null;
        private IEnumerator ShowingBossHealth()
        {
            var timer = 0f;
            while (timer < _showBossHealthTime)
            {
                yield return null;

                timer += Time.deltaTime;

                _bossAreaRoot.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, _showingCurve.Evaluate(timer/_showBossHealthTime));
            }

            _showingBossHealth = null;
        }

        private void ShowHeadShoot()
        {
            if (_showingHeadShoot != null)
                StopCoroutine(_showingHeadShoot);
            _showingHeadShoot = ShowingHeadShoot();
            StartCoroutine(_showingHeadShoot);
        }

        private IEnumerator _showingHeadShoot = null;
        private IEnumerator ShowingHeadShoot()
        {
            var score = Core.Web.InventoryController.HeadShots + 1;
            _headShootTextArea.text = "HEADSHOT!" + (score > 1 ? " x" + score.ToString() : "");
            _headShootTextArea.gameObject.SetActive(true);
            yield return new WaitForSeconds(_headShotShowTime);
            _headShootTextArea.gameObject.SetActive(false);

            _showingHeadShoot = null;
        }
    }
}