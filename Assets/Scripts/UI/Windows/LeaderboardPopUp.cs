﻿using Core.Web;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class LeaderboardPopUp : AbstractPopUp
    {
        [System.Serializable]
        public class BoardType
        {
            [SerializeField]
            private string _name = "";
            [SerializeField]
            private string _boardName = "";

            public string GetName()
            {
                return _name;
            }

            public string GetBoardName()
            {
                return _boardName;
            }
        }

        [SerializeField]
        private GameObject _unit = null;
        [SerializeField]
        private LeaderboardPopUpUnit _playerUnit = null;
        [SerializeField]
        private BoardType[] _boardTypes = new BoardType[0];
        [SerializeField]
        private TextMeshProUGUI _boardNameTextArea = null;

        private int _actualBoard = 0;
        private List<GameObject> _units = new List<GameObject>();

        public void Show()
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            base.Show();

            InitBoard(_boardTypes[_actualBoard]);
        }

        public void SetNextBoard()
        {
            _actualBoard++;
            if (_actualBoard >= _boardTypes.Length)
                _actualBoard = 0;

            InitBoard(_boardTypes[_actualBoard]);
        }

        public void SetPrevBoard()
        {
            _actualBoard--;
            if (_actualBoard < 0 )
                _actualBoard = _boardTypes.Length - 1;

            InitBoard(_boardTypes[_actualBoard]);
        }

        private void InitBoard(BoardType boardType)
        {
            _boardNameTextArea.text = boardType.GetName();
            LeaderboardController.GetLeaderboard(boardType.GetBoardName(), (result) =>
            {
                for (int i = 0; i < _units.Count; i++)
                {
                    Destroy(_units[i]);
                }

                for (int i = 0; i < result.Leaderboard.Count; i++)
                {
                    var tempUnitGO = Instantiate(_unit, _unit.transform.parent);
                    tempUnitGO.SetActive(true);
                    var tempUnit = tempUnitGO.GetComponent<LeaderboardPopUpUnit>();
                    tempUnit.Init((result.Leaderboard[i].Position + 1).ToString(), result.Leaderboard[i].DisplayName, result.Leaderboard[i].StatValue.ToString(), (result.Leaderboard[i].Position + 1) <= 3);
                    _units.Add(tempUnitGO);
                }
            });

            LeaderboardController.GetLeaderboardAroundUser(boardType.GetBoardName(), (result) =>
            {
                _playerUnit.Init((result.Leaderboard[0].Position + 1).ToString(), result.Leaderboard[0].DisplayName, result.Leaderboard[0].StatValue.ToString(), (result.Leaderboard[0].Position + 1) <= 3);
            });
        }

        public void Hide()
        {
            base.Hide();
        }
    }
}