﻿using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class ShootButton : MonoBehaviour
    {
        public static UnityEvent OnShoot = new UnityEvent();

        public void Shoot()
        {
            OnShoot.Invoke();
        }
    }
}

