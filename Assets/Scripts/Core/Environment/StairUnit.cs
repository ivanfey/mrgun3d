﻿using UnityEngine;

namespace Core.Environment
{
    public class StairUnit : MonoBehaviour
    {
        [SerializeField]
        private Transform _endPoint = null;
        [SerializeField]
        private Transform _destPoint = null;
        [SerializeField]
        private Transform _spawnPoint = null;
        [SerializeField]
        private Transform _camPointRight = null;
        [SerializeField]
        private Transform _camPointLeft = null;

        public Transform GetEndPoint()
        {
            return _endPoint;
        }

        public Transform GetDestPoint()
        {
            return _destPoint;
        }

        public Transform GetSpawnPoint()
        {
            return _spawnPoint;
        }

        public Transform GetCamPointRight()
        {
            return _camPointRight;
        }

        public Transform GetCamPointLeft()
        {
            return _camPointLeft;
        }
    }
}