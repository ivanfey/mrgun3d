﻿using Core.Web;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Windows
{
    public class PrivacyPolicyPopUp : AbstractPopUp
    {
        public static PrivacyPolicyPopUp Instance = null;
        private const string _lastVerParam = "LastVer";

        public UnityEvent OnPrivacyPolicyHided = new UnityEvent();

        [SerializeField]
        private int _defaultPPVer = 0;
        [SerializeField]
        private TextMeshProUGUI _textArea = null;
        [SerializeField]
        private Button _acceptButton = null;
        [SerializeField]
        private Button _quitButton = null;

        private int _actualPrivacyPolicy = 0;

        private void OnEnable()
        {
            Instance = this;

            _acceptButton.onClick.RemoveAllListeners();
            _acceptButton.onClick.AddListener(CustomHide);

            _quitButton.onClick.RemoveAllListeners();
            _quitButton.onClick.AddListener(()=>Application.Quit());
        }

        private void OnDisable()
        {
            Instance = null;
        }

        [ContextMenu ("Clear")]
        public void ClearPrivacyPolicyReadingInfo()
        {
            PlayerPrefs.DeleteKey(_lastVerParam);
        }

        public bool CheckAndShowDefault()
        {
            _actualPrivacyPolicy = PlayerPrefs.GetInt(_lastVerParam, -1);
            if (_defaultPPVer <= _actualPrivacyPolicy)
                return false;

            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            _actualPrivacyPolicy = _defaultPPVer;

            base.Show();
            return true;
        }

        public void CheckAndShowUpdated(UnityAction needUpdate, UnityAction noNeedUpdate)
        {
            _actualPrivacyPolicy = PlayerPrefs.GetInt(_lastVerParam, -1);
            PrivacyPolicyController.GetPrivacyPolicy((result)=> 
            {
                if(int.Parse(result.Data["PrivacyPolicyVer"]) > _actualPrivacyPolicy)
                {
                    Analytics.AnalyticsController.OpenPopUp(gameObject.name);

                    _textArea.text = result.Data["PrivacyPolicy"];
                    _actualPrivacyPolicy = int.Parse(result.Data["PrivacyPolicyVer"]);
                    needUpdate.Invoke();
                    base.Show();
                }
                else
                {
                    noNeedUpdate.Invoke();
                }
            });
        }

        private void CustomHide()
        {
            PlayerPrefs.SetInt(_lastVerParam, _actualPrivacyPolicy);
            base.Hide(()=> 
            {
                OnPrivacyPolicyHided.Invoke();
            });
        }
    }
}

