﻿using Core.InputSpace;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class PlayFabWeaponParams : MonoBehaviour
    {
        [SerializeField]
        private string _weaponItemId = "";

        public string GetWeaponId()
        {
            return _weaponItemId;
        }

        private void Start()
        {
            var tempWeaponItem = Web.CatalogController.GetWeaponCatalogItem(_weaponItemId);
            var tempCustomData = Web.CatalogController.GetCataloItemCustomData(tempWeaponItem);

            var tempCharacterInput = GetComponentInParent<CharacterInput>();
            if (tempCharacterInput != null)
            {
                tempCharacterInput.SetSpreadSpeed(float.Parse(tempCustomData["SpreadSpeed"]) * 0.01f);
                tempCharacterInput.SetSpreadHeight(float.Parse(tempCustomData["SpreadHeight"]) * 0.1f);
                tempCharacterInput.SetSpreadType(int.Parse(tempCustomData["SpreadType"]));
            }

            var tempEnemyInput = GetComponentInParent<EnemyInput>();
            if (tempEnemyInput != null)
            {

            }

            var distance = GetComponent<Distance>();
            if(distance != null)
            {
                distance.GetShoot().SetDamage(float.Parse(tempCustomData["Damage"]));
                distance.GetShoot().SetShootCount(int.Parse(tempCustomData["ShootCount"]));
                distance.GetShoot().SetShootRate(float.Parse(tempCustomData["NextShoot"]) * 0.001f);
            }

            var playerAimer = GetComponent<PlayerAimer>();
            if(playerAimer != null)
            {
                playerAimer.SetLineLenght(float.Parse(tempCustomData["LineLenght"]));
            }
        }
    }
}

