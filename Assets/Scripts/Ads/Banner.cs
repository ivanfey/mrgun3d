﻿using Core;
using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;

namespace Ads
{
    public class Banner : MonoBehaviour
    {
        public const string GameId = "3562972";
        public const string PlacementId = "Banner";
        public const bool TestMode = false;

        private void OnEnable()
        {
            MainController.PrivacyPolicyAccepted.AddListener(StartInit);
        }

        private void OnDisable()
        {
            MainController.PrivacyPolicyAccepted.RemoveListener(StartInit);
        }

        private void StartInit()
        {
            Advertisement.Initialize(GameId, TestMode);
            Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
            StartCoroutine(ShowBannerWhenReady());
        }

        IEnumerator ShowBannerWhenReady()
        {
            while (!Advertisement.IsReady(PlacementId))
                yield return null;
            Advertisement.Banner.Show(PlacementId);
        }
    }
}