﻿using System.Collections;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class Distance : Weapon
    {
        [SerializeField]
        private Shoot _shoot = null;

        public Shoot GetShoot()
        {
            return _shoot;
        }

        private void Start()
        {
            Init();

            var aimer = GetComponentInParent<CharacterAimer>();

            transform.SetParent(aimer.GetWeaponPoint());
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;

            _shoot.SetStartingParams(transform.localPosition, transform.localEulerAngles);
            _attacker.SetNewWeapon(_shoot);
        }
    }
}

