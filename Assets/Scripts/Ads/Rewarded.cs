﻿using Core;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Events;

namespace Ads
{
    public class Rewarded : MonoBehaviour, IUnityAdsListener
    {
        public static UnityEvent OnRewardedVideoReady = new UnityEvent();

        public const string GameId = "3562972";
        public const string PlacementId = "rewardedVideo";
        public const bool TestMode = false;

        private static UnityAction _onStarted = null;
        private static UnityAction _onFailed = null;
        private static UnityAction _onFinished = null;
        private static UnityAction _onSkipped = null;

        private void OnEnable()
        {
            MainController.PrivacyPolicyAccepted.AddListener(StartInit);
        }

        private void OnDisable()
        {
            MainController.PrivacyPolicyAccepted.RemoveListener(StartInit);
        }

        private void StartInit()
        {
            Advertisement.AddListener(this);
            Advertisement.Initialize(GameId, TestMode);
        }

        public static void ShowRewardedVideo(UnityAction onStarted, UnityAction onFailed, UnityAction onFinished, UnityAction onSkipped)
        {
            Debug.Log("Show rewarded video");
            Advertisement.Show(PlacementId);

            _onStarted = onStarted;
            _onFailed = onFailed;
            _onFinished = onFinished;
            _onSkipped = onSkipped;
        }

        public void OnUnityAdsDidError(string message)
        {
            Debug.LogError("Rewarded video wrong: " + message);
            _onFailed?.Invoke();
            _onFailed = null;
        }

        public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            Debug.Log(showResult);
            if (showResult == ShowResult.Finished)
            {
                Debug.Log("Rewarded video finished");
                _onFinished?.Invoke();
                _onFinished = null;
            }
            else if (showResult == ShowResult.Skipped)
            {
                Debug.Log("Rewarded video skipped");
                _onSkipped?.Invoke();
                _onSkipped = null;
            }
            else if (showResult == ShowResult.Failed)
            {
                Debug.LogError("Rewarded video wrong");
                _onFailed?.Invoke();
                _onFailed = null;
            }
        }

        public void OnUnityAdsDidStart(string placementId)
        {
            Debug.Log("Rewarded video start");
            _onStarted?.Invoke();
            _onStarted = null;
        }

        public void OnUnityAdsReady(string placementId)
        {
            Debug.Log("Rewarded video ready");
            OnRewardedVideoReady.Invoke();
        }
    }
}