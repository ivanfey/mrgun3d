﻿using Core.Character;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Windows
{
    public class BlackListPopUp : AbstractPopUp
    {
        public static BlackListPopUp Instance = null;

        [SerializeField]
        private TextMeshProUGUI _blackListTitle = null;
        [SerializeField]
        private TextMeshProUGUI _bossNames = null;
        [SerializeField]
        private Button _rewardButton = null;
        [SerializeField]
        private Button _closeButton = null;

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void CustomShow(bool canSkip)
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            _blackListTitle.text = "BLACK LIST #" + (canSkip ? BossSetter.Instance.GetActualBlackListNumber() : BossSetter.Instance.GetOldBlackListNumber());
            _bossNames.text = canSkip ? BossSetter.Instance.GetBossNames() : BossSetter.Instance.GetOldBossNames();
            _rewardButton.interactable = !canSkip;
            _closeButton.interactable = canSkip;

            base.Show();
        }

        public void Hide()
        {
            base.Hide();
        }
    }
}

