﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Effects
{
    public class EffectSpawner : MonoBehaviour
    {
        private const string _effectPath = "Effects/";
        private const float _effectLifeTime = 5f;

        public static Dictionary<string, List<EffectSpawner>> EffectsPool = new Dictionary<string, List<EffectSpawner>>();

        [SerializeField]
        private ParticleSystem _particleSystem = null;

        private string _name = "";

        public static void SpawnEffect(Vector3 pos, Vector3 forward, string name)
        {
            EffectSpawner tempEffect = null;

            if (!EffectsPool.ContainsKey(name))
                EffectsPool.Add(name, new List<EffectSpawner>());

            if (EffectsPool[name].Count > 0)
            {
                tempEffect = EffectsPool[name][0];
                EffectsPool[name].RemoveAt(0);
            }
            else
            {
                var tempEffectPref = Resources.Load<GameObject>(_effectPath + name);
                var tempEffectGO = Instantiate(tempEffectPref);
                tempEffect = tempEffectGO.GetComponent<EffectSpawner>();
            }

            tempEffect.Init(pos, forward, name);
        }

        public void Init(Vector3 pos, Vector3 forward, string name)
        {
            transform.position = pos;
            //transform.forward = forward;
            transform.up = forward;
            _name = name;
            _particleSystem.Play();
            gameObject.SetActive(true);
            StartCoroutine(DespawnEffect());
        }

        private IEnumerator DespawnEffect()
        {
            yield return new WaitForSeconds(_effectLifeTime);
            gameObject.SetActive(false);
            EffectsPool[_name].Add(this);
        }
    }
}

