﻿using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public class RealTimeStaticBatching : MonoBehaviour
    {
        [SerializeField] private bool _combineOnStart = true;
        [SerializeField] private bool _disableAutoFix = false;
        [SerializeField] private bool _skipMeshsWhichCanBeBatchedByDynamicBatching = false;
        private void Start()
        {
            if(_combineOnStart)
                CombineInternal();
        }

        [ContextMenu("Combine")]
        public void CombineInternal()
        {
            var objects = GetAllChilds<MeshFilter>(transform);
            var results = new List<GameObject>();
            
            foreach (var meshFilter in objects)
            {
                if (meshFilter.sharedMesh == null)
                {
                    continue;
                }
                if (!meshFilter.sharedMesh.isReadable)
                {
                    #if UNITY_EDITOR
                        if (!_disableAutoFix)
                        {
                            var assetPath = UnityEditor.AssetDatabase.GetAssetPath(meshFilter.sharedMesh);
                            UnityEditor.ModelImporter importerForAsset =
                                UnityEditor.AssetImporter.GetAtPath(assetPath) as UnityEditor.ModelImporter;
                            if(importerForAsset != null){
                                importerForAsset.isReadable = true;
                                importerForAsset.SaveAndReimport();
                            }
                        }

                    Debug.LogWarning(meshFilter.sharedMesh.name + " is not readable");                    
                    #endif
                    
                    continue;
                }
                if (meshFilter.gameObject.GetComponent<Renderer>() == null)
                {
                    continue;
                }

                if (meshFilter.gameObject.GetComponent<SkinnedMeshRenderer>() != null)
                {
                    continue;
                }
                if(_skipMeshsWhichCanBeBatchedByDynamicBatching)
                {
                    if (meshFilter.sharedMesh.vertices.Length < 300 && meshFilter.sharedMesh.vertexCount < 900)
                    {
                        continue;
                    }
                }

                results.Add(meshFilter.gameObject);
            }
            Debug.Log("RealStatic batching now batch "+results.Count + " objects");
            StaticBatchingUtility.Combine(results.ToArray(), this.gameObject);
        }
        private static void ProcessChild<T>(Transform aObj, ref List<T> aList) where T : Component
        {
            T c = aObj.GetComponent<T>();
            if (c != null)
                aList.Add(c);
            foreach(Transform child in aObj)
                ProcessChild<T>(child,ref aList);
        }
 
        public static T[] GetAllChilds<T>(Transform aObj) where T : Component
        {
            List<T> result = new List<T>();
            ProcessChild<T>(aObj, ref result);
            return result.ToArray();
        }

    }
}
