﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Web
{
    public class PrivacyPolicyController
    {
        public static UnityEvent OnPrivacyPolicyGettingDone = new UnityEvent();

        public static GetTitleDataResult TitleRes = null;

        public static void GetPrivacyPolicy(Action<GetTitleDataResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var keys = new List<string>();
            keys.Add("PrivacyPolicyVer");
            keys.Add("PrivacyPolicy");
            var getTitleRequest = new GetTitleDataRequest { Keys = keys };
            PlayFabClientAPI.GetTitleData(getTitleRequest, (result) =>
            {
                Debug.Log("PrivacyPolicy getting done");
                TitleRes = result;

                OnSuccess?.Invoke(result);
                OnPrivacyPolicyGettingDone.Invoke();
            }, (error)=> 
            {
                Debug.LogError("PrivacyPolicy getting failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }
    }
}