﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Environment
{
    public class StairBuilder : MonoBehaviour
    {
        public static StairBuilder Instance = null;

        [SerializeField]
        private GameObject[] _stairPrefs = new GameObject[0];
        [SerializeField]
        private StairUnit _startUnit = null;

        private StairUnit[] _stairUnits = new StairUnit[0];

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = this;
        }

        public void Spawn(int stairsCount)
        {
            _stairUnits = new StairUnit[stairsCount];
            _stairUnits[0] = _startUnit;
            for (int i = 1; i < stairsCount; i++)
            {
                var prevEndPoint = _stairUnits[i - 1].GetEndPoint();
                var tempStairGO = Instantiate(_stairPrefs[Random.Range(0, _stairPrefs.Length)], prevEndPoint.position, prevEndPoint.rotation);
                tempStairGO.transform.SetParent(transform);
                _stairUnits[i] = tempStairGO.GetComponent<StairUnit>();
            }
        }

        public StairUnit[] GetStairUnits()
        {
            return _stairUnits;
        }
    }
}