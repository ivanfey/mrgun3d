﻿using Core.Web;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

namespace UI.Windows
{
    public class SettingsPopUp : AbstractPopUp
    {
        private const string _prefMusicName = "MusicSet";
        private const string _prefSoundName = "SoundSet";
        private const string _prefShadowName = "ShadowSet";

        [SerializeField]
        private CustomToggle _musicToggle = null;
        [SerializeField]
        private CustomToggle _soundToggle = null;
        [SerializeField]
        private CustomToggle _shadowsToggle = null;
        [SerializeField]
        private TMP_InputField _nameInputField = null;

        private void OnEnable()
        {
            LoginController.OnNameChanged.AddListener(ChangeNameInTextArea);
            _nameInputField.onEndEdit.AddListener(ChangeName);
        }

        private void OnDisable()
        {
            LoginController.OnNameChanged.RemoveListener(ChangeNameInTextArea);
            _nameInputField.onEndEdit.RemoveListener(ChangeName);
        }

        private void Start()
        {
            var isMusic = PlayerPrefs.GetInt(_prefMusicName, 1) == 1;
            _musicToggle.SetToggle(isMusic);
            _musicToggle.OnCustomTriggeredEvent.RemoveAllListeners();
            _musicToggle.OnCustomTriggeredEvent.AddListener(SetMusic);
            SetMusic(isMusic);
            var isSound = PlayerPrefs.GetInt(_prefSoundName, 1) == 1;
            _soundToggle.SetToggle(isSound);
            _soundToggle.OnCustomTriggeredEvent.RemoveAllListeners();
            _soundToggle.OnCustomTriggeredEvent.AddListener(SetSounds);
            SetSounds(isSound);
            var isShadow = PlayerPrefs.GetInt(_prefShadowName, 0) == 1;
            _shadowsToggle.SetToggle(isShadow);
            _shadowsToggle.OnCustomTriggeredEvent.RemoveAllListeners();
            _shadowsToggle.OnCustomTriggeredEvent.AddListener(SetShadows);
            SetShadows(isShadow);
        }

        private void ChangeName(string newName)
        {
            if (newName == null || newName == "")
                return;
            LoginController.ChangeName(newName);
        }

        private void ChangeNameInTextArea()
        {
            if (LoginController.UserName != null)
                _nameInputField.text = LoginController.UserName;
        }

        public void Show()
        {
            base.Show(null);

            ChangeNameInTextArea();

            Analytics.AnalyticsController.OpenPopUp(gameObject.name);
        }

        public void Hide()
        {
            base.Hide();
        }

        public void SetMusic(bool state)
        {
            PlayerPrefs.SetInt(_prefMusicName, state ? 1 : 0);
            var mixer = Resources.Load<AudioMixer>("MainAudioMixer");
            mixer.SetFloat("Music", state ? 0 : -80);
        }

        public void SetSounds(bool state)
        {
            PlayerPrefs.SetInt(_prefSoundName, state ? 1 : 0);
            var mixer = Resources.Load<AudioMixer>("MainAudioMixer");
            mixer.SetFloat("Sounds", state ? 0 : -80);
        }

        public void SetShadows(bool state)
        {
            PlayerPrefs.SetInt(_prefShadowName, state ? 1 : 0);
            var lightGO = GameObject.FindGameObjectWithTag("MainLight");
            var light = lightGO.GetComponent<Light>();
            light.shadows = state ? LightShadows.Soft : LightShadows.None;
        }
    }
}

