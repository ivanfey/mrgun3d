﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.adjust.sdk;

namespace Analytics
{
    public class TempAdjust : MonoBehaviour
    {
        private void Start()
        {
            // import this package into the project:
            // https://github.com/adjust/unity_sdk/releases
#if UNITY_ANDROID
            InitAdjust("6hko8i4h4ccg");
#endif
        }

        private void InitAdjust(string adjustAppToken)
        {
            var adjustConfig = new AdjustConfig(
                adjustAppToken,
                AdjustEnvironment.Production, // AdjustEnvironment.Sandbox to test in dashboard
                true
            );
            adjustConfig.setLogLevel(AdjustLogLevel.Info); // AdjustLogLevel.Suppress to disable logs
            adjustConfig.setSendInBackground(true);
            new GameObject("Adjust").AddComponent<Adjust>(); // do not remove or rename

            // Adjust.addSessionCallbackParameter("foo", "bar"); // if requested to set session-level parameters

            //adjustConfig.setAttributionChangedDelegate((adjustAttribution) => {
            //  Debug.LogFormat("Adjust Attribution Callback: ", adjustAttribution.trackerName);
            //});

            Adjust.start(adjustConfig);
        }
    }
}

