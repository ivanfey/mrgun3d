﻿using Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Windows
{
    public class BadConnectionPopUp : AbstractPopUp
    {
        [SerializeField]
        private Button _restartButton = null;

        private void OnEnable()
        {
            _restartButton.onClick.RemoveAllListeners();
            _restartButton.onClick.AddListener(()=> 
            {
                SceneManager.LoadScene(0, LoadSceneMode.Single);
            });

            MainController.OnConnectionFailed.AddListener(Show);
        }

        private void OnDisable()
        {
            MainController.OnConnectionFailed.RemoveListener(Show);
        }

        public void Show()
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            base.Show();
        }
    }
}