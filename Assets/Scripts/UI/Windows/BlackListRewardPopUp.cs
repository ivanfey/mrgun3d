﻿using Ads;
using Core.Web;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

namespace UI.Windows
{
    public class BlackListRewardPopUp : AbstractPopUp
    {
        [SerializeField]
        private Button _spinAgainButton = null;
        [SerializeField]
        private Button _noThanksButton = null;
        [SerializeField]
        private BlackListRewardUnit[] _units = new BlackListRewardUnit[0];
        [SerializeField]
        private BlackListRewardUnit _weaponUnit = null;
        [SerializeField]
        private BlackListRewardUnit _noWeaponUnit = null;

        CatalogItem _weaponFromCatalog = null;

        public void Spin()
        {
            if (_spinning != null)
                return;
            _spinning = Spinning();
            StartCoroutine(_spinning);
        }

        private IEnumerator _spinning = null;
        private IEnumerator Spinning()
        {
            _spinAgainButton.interactable = false;
            _noThanksButton.interactable = false;

            var startDisabledUnits = new List<BlackListRewardUnit>();
            for (int i = 0; i < _units.Length; i++)
            {
                if(!_units[i].GetIsDone())
                startDisabledUnits.Add(_units[i]);
            }

            var startDisabledUnitsCount = startDisabledUnits.Count;

            yield return new WaitForSeconds(0.25f);

            var selectCount = 7;
            var speed = 0.075f;
            BlackListRewardUnit tempRandomUnit = null;
            var selectedUnitNumber = Random.Range(0, startDisabledUnits.Count);
            for (int i = 0; i <= selectCount; i++)
            {
                tempRandomUnit?.SetSelector(false);
                tempRandomUnit = startDisabledUnits[selectedUnitNumber];
                tempRandomUnit.SetSelector(true);
                yield return new WaitForSeconds(speed);
                tempRandomUnit.SetSelector(false);
                speed += 0.075f;

                selectedUnitNumber++;
                if (selectedUnitNumber >= startDisabledUnits.Count)
                    selectedUnitNumber = 0;

                if (startDisabledUnits.Count <= 1)
                    break;
            }

            tempRandomUnit.SetDone();
            switch (tempRandomUnit.GetGiftType())
            {
                case BlackListRewardUnit.GiftType.Money25:
                    InventoryController.AddMoney(25);
                    break;
                case BlackListRewardUnit.GiftType.Money50:
                    InventoryController.AddMoney(50);
                    break;
                case BlackListRewardUnit.GiftType.Money75:
                    InventoryController.AddMoney(75);
                    break;
                case BlackListRewardUnit.GiftType.Money100:
                    InventoryController.AddMoney(100);
                    break;
                case BlackListRewardUnit.GiftType.Weapon:
                    var weaponPrice = (int)_weaponFromCatalog.VirtualCurrencyPrices["CO"];
                    InventoryController.AddMoney(weaponPrice, () =>
                    {
                        InventoryController.PurchaseItem(_weaponFromCatalog);
                    });
                    break;
            }

            _spinning = null;

            if (startDisabledUnits.Count <= 1)
            {
                yield return new WaitForSeconds(0.5f);
                Hide();
            }
            else
            {
                _spinAgainButton.interactable = true;
                _noThanksButton.interactable = true;
            }
        }

        public void Show()
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            _weaponUnit.gameObject.SetActive(false);
            _noWeaponUnit.gameObject.SetActive(true);
            _units[_units.Length - 1] = _noWeaponUnit;

            _weaponFromCatalog = null;
            var weaponCatalog = CatalogController.WeaponCatalog;
            var playerInventory = InventoryController.InventoryResult;
            for (int i = 0; i < weaponCatalog.Catalog.Count; i++)
            {
                if (weaponCatalog.Catalog[i].Tags[0] == "Tier_3")
                    continue;

                var isFinded = false;
                for (int j = 0; j < playerInventory.Inventory.Count; j++)
                {
                    if (playerInventory.Inventory[j].ItemId != weaponCatalog.Catalog[i].ItemId)
                        continue;

                    isFinded = true;
                    break;
                }

                if (isFinded)
                    continue;

                _weaponFromCatalog = weaponCatalog.Catalog[i];
                _weaponUnit.gameObject.SetActive(true);
                _noWeaponUnit.gameObject.SetActive(false);
                _weaponUnit.SetGiftName(_weaponFromCatalog.DisplayName);
                _units[_units.Length - 1] = _weaponUnit;
                break;
            }

            for (int i = 0; i < _units.Length; i++)
            {
                _units[i].SetStart();
            }
            base.Show(()=> 
            {
                Spin();
            });
        }

        public void Hide()
        {
            base.Hide();
        }

        public void SpinForAds()
        {
            if (!Advertisement.IsReady(Rewarded.PlacementId))
                return;

            Rewarded.ShowRewardedVideo(null, null, () => 
            {
                Spin();
                Analytics.AnalyticsController.WatchAdsForSecondSpin();
            }, null);
        }
    }
}

