﻿using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class LeaderboardPopUpUnit : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI _placeTextArea = null;
        [SerializeField]
        private TextMeshProUGUI _nameTextArea = null;
        [SerializeField]
        private TextMeshProUGUI _scoreTextArea = null;
        [SerializeField]
        private GameObject _goldenGO = null;

        public void Init(string place, string playerName, string score, bool isGolden)
        {
            _placeTextArea.text = place;
            _nameTextArea.text = playerName;
            _scoreTextArea.text = score;
            _goldenGO.SetActive(isGolden);
        }
    }
}

