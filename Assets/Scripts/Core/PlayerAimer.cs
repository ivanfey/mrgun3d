﻿using Core.InputSpace;
using UnityEngine;

namespace Core
{
    public class PlayerAimer : MonoBehaviour
    {
        public static PlayerAimer Instance = null;

        [SerializeField]
        private LineRenderer _lineRend = null;

        public void SetLine(bool state)
        {
            _lineRend.enabled = state;
        }

        public void SetLineLenght(float value)
        {
            _lineRend.SetPosition(1, Vector3.forward * value * 0.1f);
        }

        private void OnEnable()
        {
            var player = GetComponentInParent<CharacterInput>();
            if (player != null)
                Instance = this;
        }

        private void OnDisable()
        {
            if(Instance == this)
                Instance = null;
        }
    }
}

