﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class DailyRewardUnit : MonoBehaviour
    {
        public enum RewardType
        {
            Money25,
            Money50,
            Money75,
            Money100,
            Money150,
            Money200,
            Money250,
            Weapon
        }

        [SerializeField]
        private GameObject _selectedGO = null;
        [SerializeField]
        private TextMeshProUGUI _rewardName = null;
        [SerializeField]
        private RewardType _rewardType = RewardType.Money25;

        public void SetSelected(bool state)
        {
            _selectedGO.SetActive(state);
        }

        public void SetRewardName(string newName)
        {
            if (_rewardName == null)
                return;
            _rewardName.text = newName;
        }

        public RewardType GetReward()
        {
            return _rewardType;
        }
    }
}