﻿using PlayFab;
using PlayFab.ClientModels;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Web
{
    public class GetIntEvent: UnityEvent<int> { }
    public class InventoryController
    {
        public static UnityEvent OnInventoryGetted = new UnityEvent();
        public static GetIntEvent OnMoneyChanged = new GetIntEvent();
        public static GetIntEvent OnMoneyCollectedChanged = new GetIntEvent();
        public static GetIntEvent OnHeadShotChanged = new GetIntEvent();
        public static GetIntEvent OnHeadShotCollectedChanged = new GetIntEvent();
        public static GetIntEvent OnLevelChanged = new GetIntEvent();
        public static GetIntEvent OnScoreChanged = new GetIntEvent();
        public static GetIntEvent OnScoreMaxChanged = new GetIntEvent();

        public static GetUserInventoryResult InventoryResult = null;
        public static int Money = 0;
        public static int MoneyCollected = 0;
        public static int HeadShots = 0;
        public static int HeadShotsCollected = 0;
        public static int Score = 0;
        public static int ScoreMax = 0;
        public static int Level = 0;

        public static void GetInventory(Action<GetUserInventoryResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var getUserInventoryRequest = new GetUserInventoryRequest { };
            PlayFabClientAPI.GetUserInventory(getUserInventoryRequest, (result) =>
            {
                Debug.Log("User inventory getting done");

                InventoryResult = result;
                Money = result.VirtualCurrency["CO"];
                MoneyCollected = result.VirtualCurrency["CC"];
                HeadShots = result.VirtualCurrency["HS"];
                HeadShotsCollected = result.VirtualCurrency["HC"];
                Score = result.VirtualCurrency["SC"];
                ScoreMax = result.VirtualCurrency["SM"];
                Level = result.VirtualCurrency["LV"];
                
                OnSuccess?.Invoke(result);
                OnInventoryGetted.Invoke();
                OnMoneyChanged.Invoke(Money);
                OnMoneyCollectedChanged.Invoke(MoneyCollected);
                OnHeadShotChanged.Invoke(HeadShots);
                OnHeadShotCollectedChanged.Invoke(HeadShotsCollected);
                OnLevelChanged.Invoke(Level);
                OnScoreChanged.Invoke(Score);
                OnScoreMaxChanged.Invoke(ScoreMax);
            }, (error) =>
            {
                Debug.LogError("User inventory getting failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }

        public static void AddMoney(int count, UnityAction onDone = null)
        {
            AddCurency("CO", count, (result)=>
            {
                Money = result;
                OnMoneyChanged.Invoke(Money);

                LeaderboardController.AddInLeaderboard("Money", Money);
            });
            AddCurency("CC", count, (result)=> 
            {
                MoneyCollected = result;
                OnMoneyCollectedChanged.Invoke(MoneyCollected);
                onDone?.Invoke();
            });
        }

        public static void AddHeadShots(int count, Action OnSuccess = null)
        {
            AddCurency("HS", count, (result)=> 
            {
                HeadShots = result;
                OnHeadShotChanged.Invoke(HeadShots);
                OnSuccess?.Invoke();
            });
            AddCurency("HC", count, (result) => 
            {
                HeadShotsCollected = result;
                OnHeadShotCollectedChanged.Invoke(HeadShotsCollected);

                LeaderboardController.AddInLeaderboard("Headshoot", HeadShotsCollected);
            });
        }

        public static void AddLevel(int count)
        {
            AddCurency("LV", count, (result) => 
            {
                Level = result;
                OnLevelChanged.Invoke(Level);

                LeaderboardController.AddInLeaderboard("Level", Level);
            });
        }

        public static void AddScore(int count)
        {
            AddCurency("SC", count, (result) =>
            {
                Score = result;
                if(Score <= ScoreMax)
                {
                    OnScoreChanged.Invoke(Score);
                }
                else
                {
                    SubtractCurency("SM", ScoreMax, (resultMax) =>
                    {
                        AddCurency("SM", Score, (newMaxScore)=> 
                        {
                            ScoreMax = newMaxScore;
                            OnScoreChanged.Invoke(Score);
                            OnScoreMaxChanged.Invoke(ScoreMax);

                            LeaderboardController.AddInLeaderboard("Score", ScoreMax);
                        });
                    });
                }
            });
        }

        private static void AddCurency(string code, int count, Action<int> OnSuccess = null, Action OnFailed = null)
        {
            var addVCRequest = new PlayFab.ServerModels.AddUserVirtualCurrencyRequest { PlayFabId = LoginController.LoginRes.PlayFabId, VirtualCurrency = code, Amount = count };
            PlayFabServerAPI.AddUserVirtualCurrency(addVCRequest, (result) =>
            {
                Debug.Log("Add "+ code + " done");
                OnSuccess?.Invoke(result.Balance);
            }, (error) =>
            {
                Debug.LogError("Add " + code + " failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke();
            });
        }

        public static void SubtractAllHeadShots()
        {
            SubtractCurency("HS", HeadShots, (result) =>
            {
                HeadShots = result;
                OnHeadShotChanged.Invoke(HeadShots);
            });
        }

        public static void SubtractAllScore()
        {
            SubtractCurency("SC", Score, (result) =>
            {
                Score = result;
                OnScoreChanged.Invoke(Score);
            });
        }

        private static void SubtractCurency(string code, int count, Action<int> OnSuccess = null, Action OnFailed = null)
        {
            if(count <= 0)
            {
                Debug.Log("Subtract " + code + " done");
                OnSuccess?.Invoke(InventoryResult.VirtualCurrency[code]);
                return;
            }
            var subtractVCRequest = new PlayFab.ServerModels.SubtractUserVirtualCurrencyRequest { PlayFabId = LoginController.LoginRes.PlayFabId, VirtualCurrency = code, Amount = count };
            PlayFabServerAPI.SubtractUserVirtualCurrency(subtractVCRequest, (result) =>
            {
                Debug.Log("Subtract " + code + " done");
                OnSuccess?.Invoke(result.Balance);
            }, (error) =>
            {
                Debug.LogError("Subtract " + code + " failed");
                Debug.LogError(error.ErrorMessage);
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke();
            });
        }

        public static void PurchaseItem(CatalogItem item, Action<GetUserInventoryResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var buyRequest = new PurchaseItemRequest { ItemId = item.ItemId, CatalogVersion = item.CatalogVersion, VirtualCurrency = "CO", Price = (int)item .VirtualCurrencyPrices["CO"]};
            PlayFabClientAPI.PurchaseItem(buyRequest, (buyResult) =>
            {
                GetInventory((result)=> 
                {
                    Debug.Log("Buy " + item.DisplayName + " done");
                    OnSuccess?.Invoke(result);
                },(error)=> 
                {
                    OnFailed?.Invoke(error);
                });
            }, (error) =>
            {
                Debug.LogError("Buy " + item.DisplayName + " failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }
    }
}