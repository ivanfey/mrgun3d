﻿using Newtonsoft.Json;
using PlayFab;
using PlayFab.ClientModels;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Web
{
    public class CatalogController : MonoBehaviour
    {
        public static UnityEvent OnWeaponCatalogGetDone = new UnityEvent();
        public static UnityEvent OnSkinCatalogGetDone = new UnityEvent();

        public static GetCatalogItemsResult WeaponCatalog = null;
        public static GetCatalogItemsResult SkinCatalog = null;

        public static void GetWeaponCatalog(Action<GetCatalogItemsResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            GetCatalog("Weapons_0", (result) =>
            {
                WeaponCatalog = result;
                OnWeaponCatalogGetDone.Invoke();
                OnSuccess?.Invoke(result);
            }, OnFailed);
        }

        public static void GetSkinCatalog(Action<GetCatalogItemsResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            GetCatalog("Skins_0", (result) =>
            {
                SkinCatalog = result;
                OnSkinCatalogGetDone.Invoke();
                OnSuccess?.Invoke(result);
            }, OnFailed);
        }

        private static void GetCatalog(string catalogVersion, Action<GetCatalogItemsResult> OnSuccess = null, Action<PlayFabError> OnFailed = null)
        {
            var weaponCatalogRequest = new GetCatalogItemsRequest { CatalogVersion = catalogVersion };
            PlayFabClientAPI.GetCatalogItems(weaponCatalogRequest, (result) =>
            {
                Debug.Log(catalogVersion + " catalog getting done");
                OnSuccess?.Invoke(result);
            }, (error) =>
            {
                Debug.LogError(catalogVersion + " catalog getting failed");
                MainController.OnConnectionFailed.Invoke();
                OnFailed?.Invoke(error);
            });
        }

        public static CatalogItem GetWeaponCatalogItem(string weaponId)
        {
            return GetCatalogItemById(WeaponCatalog.Catalog, weaponId);
        }

        public static CatalogItem GetSkinCatalogItem(string skinId)
        {
            return GetCatalogItemById(SkinCatalog.Catalog, skinId);
        }

        private static CatalogItem GetCatalogItemById(List<CatalogItem> catalog, string itemId)
        {
            for (int i = 0; i < catalog.Count; i++)
            {
                if (itemId != catalog[i].ItemId)
                    continue;
                return catalog[i];
            }
            return null;
        }

        public static Dictionary<string, string> GetCataloItemCustomData(CatalogItem catalogItem)
        {
            var tempCustomDataText = catalogItem.CustomData;
            var tempCustomData = JsonConvert.DeserializeObject<Dictionary<string, string>>(tempCustomDataText);
            return tempCustomData;
        }
    }
}

