﻿
namespace UI.Windows
{
    public class EliteAccessPopUp : AbstractPopUp
    {
        public void Show()
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            base.Show();
        }

        public void Hide()
        {
            base.Hide();
        }
    }
}

