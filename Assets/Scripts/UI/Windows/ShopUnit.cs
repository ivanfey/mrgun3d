﻿using Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Newtonsoft.Json;
using PlayFab.ClientModels;
using UnityEngine.UI;

namespace UI.Windows
{
    public class ShopUnit : MonoBehaviour
    {
        [System.Serializable]
        public class CustomDataParams
        {
            public string ParamName = "";
            public string ParamValue = "";
            public TextMeshProUGUI ParamTextArea = null;
        }

        public enum UnitState
        {
            Disabled,
            Enabled
        }

        [SerializeField]
        private TextMeshProUGUI _nameTextArea = null;

        [SerializeField]
        private GameObject _enabledGO = null;
        [SerializeField]
        private GameObject _selectedGO = null;
        [SerializeField]
        private GameObject _buingSelector = null;
        [SerializeField]
        private Button _infoButton = null;

        private ShopMenuWindow.ShopSegment.UnitType _unitType = ShopMenuWindow.ShopSegment.UnitType.Weapon;
        private UnitState _unitState = UnitState.Disabled;

        [SerializeField]
        private CustomDataParams[] _customDataParams = new CustomDataParams[0];

        private CatalogItem _catalogItem = null;
        private ShopMenuWindow _shopMenuWindow = null;
        private ShopMenuWindow.ShopSegment _shopSegment = null;

        public UnitState GetUnitState()
        {
            return _unitState;
        }

        public ShopMenuWindow.ShopSegment.UnitType GetUnitType()
        {
            return _unitType;
        }

        public CatalogItem GetCatalogItem()
        {
            return _catalogItem;
        }

        public void Init(ShopMenuWindow shopMenuWindow, ShopMenuWindow.ShopSegment shopSegment, CatalogItem catalogItem, UnitState state, ShopMenuWindow.ShopSegment.UnitType unitType)
        {
            _shopMenuWindow = shopMenuWindow;
            _unitState = state;
            _unitType = unitType;

            _catalogItem = catalogItem;
            _shopSegment = shopSegment;

            _nameTextArea.text = catalogItem.DisplayName;

            var customDataText = catalogItem.CustomData;
            if (customDataText != null)
            {
                var customData = JsonConvert.DeserializeObject<Dictionary<string, string>>(customDataText);
                for (int i = 0; i < _customDataParams.Length; i++)
                {
                    _customDataParams[i].ParamValue = customData[_customDataParams[i].ParamName];
                    if(_customDataParams[i].ParamTextArea != null)
                        _customDataParams[i].ParamTextArea.text = _customDataParams[i].ParamValue;
                }
            }

            _infoButton.onClick.RemoveAllListeners();
            _infoButton.onClick.AddListener(() =>
            {
                switch (_unitType)
                {
                    case ShopMenuWindow.ShopSegment.UnitType.Weapon:
                        WeaponShower.Instance.ShowWeapon(_catalogItem.ItemId);
                        WeaponShopUnitInfoPopUp.Instance.Show(_catalogItem.DisplayName, _catalogItem.Description, _customDataParams);
                        break;
                    case ShopMenuWindow.ShopSegment.UnitType.Outfit:
                        OutfitShower.Instance.ShowOutfit(_catalogItem.ItemId);
                        OutfitShopUnitInfoPopUp.Instance.Show(_catalogItem.DisplayName, _catalogItem.Description);
                        break;
                }
            });

            switch (_unitState)
            {
                case UnitState.Disabled:
                    _enabledGO.SetActive(false);
                    break;
                case UnitState.Enabled:
                    _enabledGO.SetActive(true);
                    break;
            }

            Deselect();
        }

        public void Select()
        {
            if (_shopSegment.GetActualUnit() != null)
                _shopSegment.GetActualUnit().Deselect();

            _selectedGO.SetActive(true);

            _shopSegment.SetActualUnit(this);

            PlayerPrefs.SetInt(_shopSegment.GetLastSelectedSave(), _shopSegment.GetUnitArrayId(this));

            switch (_unitType)
            {
                case ShopMenuWindow.ShopSegment.UnitType.Weapon:
                    MainController.ChangePlayerWeapon(_catalogItem.ItemId);
                    WeaponShower.Instance.ShowWeapon(_catalogItem.ItemId);
                    PlayerPrefs.SetString(MainController.WeaponInHandsSave, _catalogItem.ItemId);
                    break;
                case ShopMenuWindow.ShopSegment.UnitType.Outfit:
                    MainController.ChangePlayerOutfit(_catalogItem.ItemId);
                    OutfitShower.Instance.ShowOutfit(_catalogItem.ItemId);
                    PlayerPrefs.SetString(MainController.OutfitOnCharacterSave, _catalogItem.ItemId);
                    break;
            }
        }

        public void Deselect()
        {
            _selectedGO.SetActive(false);
        }

        public GameObject GetBuingSelector()
        {
            return _buingSelector;
        }

        public void Buy()
        {
            if (_buing != null)
                return;

            _buing = Buing();
            StartCoroutine(_buing);
        }

        private IEnumerator _buing = null;
        public IEnumerator Buing()
        {
            var isDone = false;
            Core.Web.InventoryController.PurchaseItem(_catalogItem, (result)=> isDone = true);
            while (!isDone)
                yield return null;

            Select();
            yield return null;

            _shopSegment.Init(_shopMenuWindow);

            switch (_unitType)
            {
                case ShopMenuWindow.ShopSegment.UnitType.Weapon:
                    Analytics.AnalyticsController.BuyWeapon(_catalogItem.ItemId);
                    break;
                case ShopMenuWindow.ShopSegment.UnitType.Outfit:
                    Analytics.AnalyticsController.BuyOutfit(_catalogItem.ItemId);
                    break;
            }

            _buing = null;
        }
    }
}