﻿using UnityEngine;
using UnityEngine.AI;
using Core.Character;
using Core.Character.Weapons;
using Utils;

namespace Core.InputSpace
{
    public class EnemyInput : MonoBehaviour
    {
        [SerializeField]
        private float[] _states = new float[0];

        private Animator _anim = null;
        private NavMeshAgent _navAgent = null;
        private CharacterMover _charMover = null;
        private CharacterStats _charStats = null;
        private CharacterAttacker _charAttacker = null;
        private WeaponGetter _weaponGetter = null;

        private bool _isAttack = false;

        private void OnDisable()
        {
            _navAgent.enabled = false;
        }

        public void StartInit()
        {
            _anim = GetComponent<Animator>();
            _navAgent = GetComponent<NavMeshAgent>();
            _charMover = GetComponent<CharacterMover>();
            _charStats = GetComponent<CharacterStats>();
            _charAttacker = GetComponent<CharacterAttacker>();
            _weaponGetter = GetComponent<WeaponGetter>();

            _weaponGetter.SetWeapon(transform);

            var realStaticBatcher = GetComponentInChildren<RealTimeStaticBatching>(true);
            realStaticBatcher.CombineInternal();
        }

        public void Init(Vector3 position)
        {  
            _charMover.SetState(_states[Random.Range(0, _states.Length)]);
            transform.position = position;
            var navHit = new NavMeshHit();
            if (NavMesh.SamplePosition(position, out navHit, float.MaxValue, -1))
                transform.position = navHit.position;

            gameObject.SetActive(true);
            _navAgent.enabled = true;
        }

        public CharacterStats GetCharStats()
        {
            return _charStats;
        }

        public Animator GetAnim()
        {
            return _anim;
        }

        public void SetDest(Vector3 destPos)
        {
            _navAgent.SetDestination(destPos);
        }

        public void Shoot()
        {
            _isAttack = true;
        }

        private void Update()
        {
            var isHit = _anim.GetCurrentAnimatorStateInfo(0).IsTag("Hit") || _anim.GetNextAnimatorStateInfo(0).IsTag("Hit");
            _anim.applyRootMotion = isHit;
            if(_navAgent.enabled)
                _navAgent.isStopped = isHit;

            var moveInput = transform.InverseTransformDirection(_navAgent.velocity);
            var targetPos = CharacterInput.PlayerInput.GetAnim().GetBoneTransform(HumanBodyBones.Chest).position;

            var isSimpleRot = true;
            _charMover.SetInputs(moveInput, targetPos, 1f, isSimpleRot);

            _charAttacker.SetInputs(_isAttack);
            _isAttack = false;

            if (!_anim.enabled)
                enabled = false;
        }
    }
}