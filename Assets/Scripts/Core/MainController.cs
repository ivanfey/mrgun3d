﻿using Core.Character;
using Core.Character.Weapons;
using Core.Environment;
using Core.InputSpace;
using Effects;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UI.Windows;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using Utils;

namespace Core
{
    public class MainController : MonoBehaviour
    {
        public const string WeaponInHandsSave = "ActualWeapon";
        public const string FirstWeaponInHandId = "Weapon_0_0";
        public const string OutfitOnCharacterSave = "ActualOutfit";
        public const string FirstOutfitOnCharacterId = "Outfit_0_0_Character_Bandit_Male_01";

        [Serializable]
        public class BossSpawned : UnityEvent<GameObject> { }

        public static UnityEvent PrivacyPolicyAccepted = new UnityEvent();
        public static BossSpawned OnBossSpawned = new BossSpawned();
        public static UnityEvent OnHeadInpact = new UnityEvent();
        public static UnityEvent OnBodyInpact = new UnityEvent();
        public static UnityEvent OnConnectionFailed = new UnityEvent();

        public static bool MainInitDone = false;

        [SerializeField]
        private GameObject _splashScreen = null;
        [SerializeField]
        private TextMeshProUGUI _loadingtextArea = null;
        [SerializeField]
        private int _preBossEnemyesCount = 7;

        private static float _inhLevel = 0f;
        
        [SerializeField]
        private int _actualStairUnit = 0;
        [SerializeField]
        private GameObject _enemyPref = null;
        [SerializeField]
        private GameObject _bossPref = null;

        public static bool IsStart = false;
        public static bool IsAdsWatched = false;

        private CharacterInput _playerInput = null;

        [ContextMenu("Clear all prefs")]
        public void ClearAllPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
        }

        public static void ChangePlayerWeapon(string weaponId)
        {
            var weaponPref = Resources.Load<GameObject>("Weapons/" + weaponId);
            var oldWeapon = CharacterInput.PlayerInput.transform.GetComponentInChildren<Weapon>();
            if (oldWeapon != null)
                Destroy(oldWeapon.gameObject);
            var tempWeaponGO = Instantiate(weaponPref, CharacterInput.PlayerInput.transform);
            var realStaticBatcher = tempWeaponGO.GetComponentInChildren<RealTimeStaticBatching>(true);
            realStaticBatcher.CombineInternal();
        }
        public static void ChangePlayerOutfit(string outfitId)
        {
            SkinChanger.Instance.SetNewSkin(outfitId);
        }

        public static float GetInhLevel()
        {
            return _inhLevel;
        }

        private void OnEnable()
        {
            Bullet.OnEnemyHead.AddListener(HeadShot);
            Bullet.OnEnemyBody.AddListener(BodyShot);
            if (_game != null)
                StopCoroutine(_game);

            _game = Game();
            StartCoroutine(_game);
        }

        private void OnDisable()
        {
            Bullet.OnEnemyHead.RemoveListener(HeadShot);
            Bullet.OnEnemyBody.RemoveListener(BodyShot);

            if (_game != null)
                StopCoroutine(_game);
        }

        public void HeadShot()
        {
            OnHeadInpact.Invoke();

            Web.InventoryController.AddMoney(1);
            Web.InventoryController.AddScore(Web.InventoryController.Level * 2);
            Web.InventoryController.AddHeadShots(1);
        }

        public void BodyShot()
        {
            Web.InventoryController.SubtractAllHeadShots();
            Web.InventoryController.AddScore(Web.InventoryController.Level);

            OnBodyInpact.Invoke();
        }

        private IEnumerator _game = null;
        private IEnumerator Game()
        {
            MainInitDone = false;

            _loadingtextArea.text = "Loading: first init...";

            Bullet.BulletsPool = new Dictionary<string, List<Bullet>>();
            Bullet.Impulses = new List<GameObject>();
            EffectSpawner.EffectsPool = new Dictionary<string, List<EffectSpawner>>();

            IsStart = false;
            IsAdsWatched = false;
            _inhLevel = 0f;
            _splashScreen.SetActive(true);

            yield return null;

            _loadingtextArea.text = "Loading: first privacy policy check...";

            if (PrivacyPolicyPopUp.Instance.CheckAndShowDefault())
            {
                var isPPAccepted = false;
                PrivacyPolicyPopUp.Instance.OnPrivacyPolicyHided.RemoveAllListeners();
                PrivacyPolicyPopUp.Instance.OnPrivacyPolicyHided.AddListener(()=>isPPAccepted = true);
                while (!isPPAccepted)
                    yield return null;
            }

            _loadingtextArea.text = "Loading: first privacy policy check...";

            var isDone = false;
            Web.LoginController.Login((result)=> isDone = true);
            while(!isDone)
                yield return null;

            _loadingtextArea.text = "Loading: second privacy policy check...";

            var isPPUpdated = false;
            PrivacyPolicyPopUp.Instance.CheckAndShowUpdated(()=> 
            {
                PrivacyPolicyPopUp.Instance.OnPrivacyPolicyHided.RemoveAllListeners();
                PrivacyPolicyPopUp.Instance.OnPrivacyPolicyHided.AddListener(() => isPPUpdated = true);
            }, ()=> 
            {
                isPPUpdated = true;
            });
            while (!isPPUpdated)
                yield return null;

            PrivacyPolicyAccepted.Invoke();

            _loadingtextArea.text = "Loading: weapon catalog getting...";

            isDone = false;
            Web.CatalogController.GetWeaponCatalog((result) => isDone = true);
            while (!isDone)
                yield return null;

            _loadingtextArea.text = "Loading: skin catalog getting...";

            isDone = false;
            Web.CatalogController.GetSkinCatalog((result) => isDone = true);
            while (!isDone)
                yield return null;

            _loadingtextArea.text = "Loading: new player init...";

            if (Web.LoginController.LoginRes.NewlyCreated)
            {
                isDone = false;
                Web.InventoryController.PurchaseItem(Web.CatalogController.GetWeaponCatalogItem(FirstWeaponInHandId), (result)=> isDone = true);
                while (!isDone)
                    yield return null;
                isDone = false;
                Web.InventoryController.PurchaseItem(Web.CatalogController.GetSkinCatalogItem(FirstOutfitOnCharacterId), (result) => isDone = true);
                while (!isDone)
                    yield return null;
            }

            _loadingtextArea.text = "Loading: player inventory getting...";

            isDone = false;
            Web.InventoryController.GetInventory((result)=> isDone = true);
            while (!isDone)
                yield return null;

            _loadingtextArea.text = "Loading: player input init...";

            _playerInput = CharacterInput.PlayerInput;

            ChangePlayerWeapon(PlayerPrefs.GetString(WeaponInHandsSave, FirstWeaponInHandId));
            ChangePlayerOutfit(PlayerPrefs.GetString(OutfitOnCharacterSave, FirstOutfitOnCharacterId));

            _loadingtextArea.text = "Loading: blacklist init...";

            bool isBlackListDone = false;
            var blackList = BossSetter.Instance.GetOrRandomize(5, out isBlackListDone);

            _loadingtextArea.text = "Loading: boss init...";

            BossSetter.BlackList.BossType actualBossType = null;
            for (int i = 0; i < blackList.Bosses.Count; i++)
            {
                if (blackList.Bosses[i].IsDie)
                    continue;
                actualBossType = blackList.Bosses[i];
                break;
            }
            
            var bossHealth = Mathf.Clamp(Web.InventoryController.Level, 1, 50);
            StairBuilder.Instance.Spawn(_preBossEnemyesCount + bossHealth + 2);
            var bossGO = Instantiate(_bossPref);
            var bossSkinSetter = bossGO.GetComponent<BossSkinChanger>();
            bossSkinSetter.SetRandomSkin(actualBossType.IsMale, actualBossType.SkinId);
            var bossInput = bossGO.GetComponent<EnemyInput>();
            bossInput.StartInit();
            var bossCharacterStats = bossInput.GetCharStats();
            bossCharacterStats.MainStats.MaxHealth = bossHealth;
            bossCharacterStats.MainStats.Health = bossCharacterStats.MainStats.MaxHealth;
            bossInput.Init(StairBuilder.Instance.GetStairUnits()[_actualStairUnit].GetDestPoint().position);

            _loadingtextArea.text = "Loading: enemy pool init...";

            var enemyInputs = new List<EnemyInput>();
            EnemyInput tempEnemyInput = null;
            for (int i = 0; i < _preBossEnemyesCount + 1; i++)
            {
                var enemyGO = Instantiate(_enemyPref);
                tempEnemyInput = enemyGO.GetComponent<EnemyInput>();
                tempEnemyInput.StartInit();
                enemyInputs.Add(tempEnemyInput);
            }

            _playerInput.SetTargetPos(bossInput.GetAnim().GetBoneTransform(HumanBodyBones.Chest).position, Time.time, false);

            _splashScreen.SetActive(false);

            MainInitDone = true;

            WindowController.Instance.SetNewWindow("Main");

            if (DailyRewardPopUp.Instance.CustomShow())
            {
                isDone = false;
                DailyRewardPopUp.Instance.OnHided.RemoveAllListeners();
                DailyRewardPopUp.Instance.OnHided.AddListener(() => isDone = true);
                while (!isDone)
                    yield return null;
            }

            yield return null;
            if (isBlackListDone)
                BlackListPopUp.Instance.CustomShow(false);

            while (!IsStart)
                yield return null;

            WindowController.Instance.SetNewWindow("Battle");

            bossInput.SetDest(StairBuilder.Instance.GetStairUnits()[_actualStairUnit].GetSpawnPoint().position);

            yield return new WaitForSeconds(1f);
            
            bossGO.SetActive(false);

            EnemyInput enemy = null;
            var isBoss = false;
            while (_playerInput.GetAnim().enabled)
            {
                yield return null;
                
                var actualStairUnit = StairBuilder.Instance.GetStairUnits()[_actualStairUnit];
                var nextStairUnit = StairBuilder.Instance.GetStairUnits()[_actualStairUnit + 1];

                var destPoint = actualStairUnit.GetDestPoint();

                _inhLevel = (float)(_actualStairUnit) / (float)(_preBossEnemyesCount);

                if (_preBossEnemyesCount < _actualStairUnit)
                    isBoss = true;

                if (enemy == null)
                {
                    if(isBoss)
                    {
                        bossInput.Init(nextStairUnit.GetSpawnPoint().position);
                        enemy = bossInput;
                        OnBossSpawned.Invoke(bossInput.gameObject);
                    }
                    else
                    {
                        enemyInputs[0].Init(nextStairUnit.GetSpawnPoint().position);
                        enemy = enemyInputs[0];
                        enemyInputs.RemoveAt(0);
                    }
                }

                var enemyChestTrans = enemy.GetAnim().GetBoneTransform(HumanBodyBones.Chest);

                yield return null;

                enemy.SetDest(nextStairUnit.GetDestPoint().position);

                _playerInput.SetDest(destPoint.position);
                var campointTrans = actualStairUnit.GetCamPointRight();
                if (_actualStairUnit % 2 != 0)
                    campointTrans = actualStairUnit.GetCamPointLeft();
                CameraInput.Instance.MoveTo(campointTrans.position, campointTrans.rotation, null);
                var dist = float.MaxValue;
                while (dist > 1f)
                {
                    yield return null;
                    dist = Vector3.Distance(destPoint.position, _playerInput.transform.position);

                    _playerInput.SetLookAtWeight(Mathf.Clamp01(dist));

                    if (dist > 3)
                    {
                        _playerInput.SetTargetPos(_playerInput.transform.position + _playerInput.transform.forward * 15f, Time.time, false);
                    }
                    else
                    {
                        _playerInput.SetTargetPos(enemyChestTrans.position, Time.time, false);
                    }
                }

                yield return new WaitForSeconds(0.5f);
                
                PlayerAimer.Instance.SetLine(true);
                var shoot = false;
                UI.ShootButton.OnShoot.RemoveAllListeners();
                UI.ShootButton.OnShoot.AddListener(()=> shoot = true);
                var startTime = Time.time;
                while (!shoot)
                {
                    yield return null;
                    _playerInput.SetTargetPos(destPoint.position + destPoint.forward * 5f + Vector3.up * 1.5f, startTime, true);
                }
                UI.ShootButton.OnShoot.RemoveAllListeners();
                PlayerAimer.Instance.SetLine(false);

                var isDamaged = false;
                var isDie = false;

                UnityAction onDamageCall = () => isDamaged = true;
                UnityAction onDieCall = () => isDie = true;
                enemy.GetCharStats().OnDamaged.AddListener(onDamageCall);
                enemy.GetCharStats().OnDie.AddListener(onDieCall);

                _playerInput.Shoot();

                var timer = 0f;
                while (timer < 1.75f)
                {
                    yield return null;
                    timer += Time.deltaTime;
                }

                if (!isDamaged)
                {
                    enemy.Shoot();
                    Analytics.AnalyticsController.Die();

                    yield return new WaitForSeconds(3f);

                    Web.InventoryController.SubtractAllHeadShots();
                                        
                    if(!IsAdsWatched)
                    {
                        WindowController.Instance.SetNewWindow("AfterDead");

                        AfterDeadMenuWindow.OnAdsDone.AddListener(() => IsAdsWatched = true);
                        while (!IsAdsWatched)
                            yield return null;
                        
                        _actualStairUnit--;
                        var tempCharacterPref = Resources.Load<GameObject>("Characters/CharacterWestern");
                        var tempCharacterGO = Instantiate(tempCharacterPref, actualStairUnit.GetSpawnPoint().position, actualStairUnit.GetSpawnPoint().rotation);
                        
                        ChangePlayerWeapon(PlayerPrefs.GetString(WeaponInHandsSave, FirstWeaponInHandId));
                        ChangePlayerOutfit(PlayerPrefs.GetString(OutfitOnCharacterSave, FirstOutfitOnCharacterId));

                        _playerInput = CharacterInput.PlayerInput;
                        _playerInput.SetDest(actualStairUnit.GetDestPoint().position);
                        _playerInput.SetTargetPos(enemyChestTrans.position, Time.time, false);

                        WindowController.Instance.SetNewWindow("Battle");
                    }
                    else
                    {
                        WindowController.Instance.SetNewWindow("AfterAds");
                    }
                }
                else
                {
                    if (isDie)
                    {
                        enemy = null;
                        if (isBoss)
                        {
                            Web.InventoryController.AddMoney(3);
                            Web.InventoryController.AddLevel(1);
                            Analytics.AnalyticsController.KillBoss();

                            _playerInput.SetDest(nextStairUnit.GetSpawnPoint().position);
                            _playerInput.SetTargetPos(nextStairUnit.GetSpawnPoint().position, Time.time, false);
                            actualBossType.IsDie = true;
                            yield return new WaitForSeconds(3f);
                            SceneManager.LoadScene(0, LoadSceneMode.Single);
                            break;
                        }
                        else
                        {
                            Analytics.AnalyticsController.KillEnemy();
                        }
                    }
                }

                enemy?.GetCharStats().OnDamaged.RemoveListener(onDamageCall);
                enemy?.GetCharStats().OnDie.RemoveListener(onDieCall);

                _actualStairUnit++;
            }
        }
    }
}