﻿using UnityEngine;

namespace Core.Character.Weapons
{
    public class VirtualHand : MonoBehaviour
    {
        [SerializeField]
        private Animator _anim = null;

        private void LateUpdate()
        {
            var rightHandTrans = _anim.GetBoneTransform(HumanBodyBones.RightHand);
            transform.position = rightHandTrans.position;
            transform.rotation = rightHandTrans.rotation;
        }
    }
}