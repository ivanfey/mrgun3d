﻿using TMPro;
using UnityEngine;

namespace Effects
{
    public class TextBlinker : MonoBehaviour
    {
        [SerializeField]
        private float _amplitude = 0.5f;
        [SerializeField]
        private TextMeshProUGUI _textArea = null;
        [SerializeField]
        private Color _defaultColor = Color.white;

        private void Update()
        {
            _textArea.color = Color.Lerp(Color.clear, _defaultColor, (Mathf.Sin(Time.time * _amplitude) + 1f) * 0.5f);
        }
    }
}

