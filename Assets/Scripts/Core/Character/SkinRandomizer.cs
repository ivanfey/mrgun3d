﻿using UnityEngine;

namespace Core.Character
{
    public class SkinRandomizer : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _skins = new GameObject[0];

        private void Awake()
        {
            var randomSkin = Random.Range(0, _skins.Length);
            for (int i = 0; i < _skins.Length; i++)
            {
                _skins[i].SetActive(i == randomSkin);
            }
        }
    }
}

