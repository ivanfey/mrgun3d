﻿using UnityEngine;

namespace Core.Character
{
    [RequireComponent(typeof(CharacterMover))]
    public class CharacterAimer : MonoBehaviour
    {
        [SerializeField]
        private Transform _chestAimingPoint = null;
        [SerializeField]
        private Transform _handAimingPoint = null;
        [SerializeField]
        private Transform _weaponAimingPoint = null;
        [SerializeField]
        private float _aimingSpeed = 5f;

        private CharacterMover _charMover = null;

        private void Start()
        {
            _charMover = GetComponent<CharacterMover>();
        }

        private void LateUpdate()
        {
            var speed = Time.deltaTime * _aimingSpeed;
            var lookAtPoint = _charMover.GetTargetPos();

            var oldRot = _chestAimingPoint.localRotation;
            _chestAimingPoint.LookAt(lookAtPoint);
            _chestAimingPoint.localRotation = Quaternion.Lerp(oldRot, _chestAimingPoint.localRotation, speed);

            oldRot = _handAimingPoint.localRotation;
            _handAimingPoint.LookAt(lookAtPoint);
            _handAimingPoint.localRotation = Quaternion.Lerp(oldRot, _handAimingPoint.localRotation, speed);

            oldRot = _weaponAimingPoint.localRotation;
            _weaponAimingPoint.LookAt(lookAtPoint);
            _weaponAimingPoint.localRotation = Quaternion.Lerp(oldRot, _weaponAimingPoint.localRotation, speed);
        }

        public Transform GetWeaponPoint()
        {
            return _weaponAimingPoint;
        }
    }
}

