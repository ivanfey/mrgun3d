﻿using UnityEngine;
using UnityEngine.AI;
using Core.Character;

namespace Core.InputSpace
{
    public class CharacterInput : MonoBehaviour
    {
        public static CharacterInput PlayerInput = null;
        
        [SerializeField]
        private float _animState = 2f;
        [SerializeField]
        private AnimationCurve[] _spreadTypes = new AnimationCurve[0];
        

        private NavMeshAgent _navAgent = null;
        private CharacterMover _charMover = null;
        private CharacterStats _charStats = null;
        private CharacterAttacker _charAttacker = null;
        private Animator _anim = null;

        private bool _isAttack = false;
        private Vector3 _targetPos = Vector3.zero;
        private float _lookAtWeight = 1f;
        [SerializeField]
        private float _spreadSpeed = 0f;
        [SerializeField]
        private float _spreadHeight = 0f;
        [SerializeField]
        private int _spreadType = 0;

        public void SetSpreadType(int value)
        {
            _spreadType = value;
        }

        public void SetSpreadHeight(float value)
        {
            _spreadHeight = value;
        }

        public void SetSpreadSpeed(float value)
        {
            _spreadSpeed = value;
        }

        private void OnEnable()
        {
            PlayerInput = this;
        }

        private void OnDisable()
        {
            PlayerInput = null;
        }

        private void Awake()
        {
            _anim = GetComponent<Animator>();
            _navAgent = GetComponent<NavMeshAgent>();
            _charMover = GetComponent<CharacterMover>();
            _charStats = GetComponent<CharacterStats>();
            _charAttacker = GetComponent<CharacterAttacker>();

            _navAgent.speed = _charStats.MoveStats.MoveSpeed;

            _charMover.SetState(_animState);
        }

        public Animator GetAnim()
        {
            return _anim;
        }

        public void Shoot()
        {
            _isAttack = true;
        }

        public void SetDest(Vector3 destPos)
        {
            _navAgent.SetDestination(destPos);
        }

        public void SetTargetPos(Vector3 newPos, float startTime, bool isSpread)
        {
            _targetPos = newPos;
            if (!isSpread)
                return;

            var time = (Time.time - startTime) * _spreadSpeed;
            _targetPos += Vector3.up * _spreadHeight * _spreadTypes[_spreadType].Evaluate(time % 1); //_targetPos += Vector3.up * ((Mathf.Sin((startTime - Time.time) * _spreadSpeed) + 1) * 0.5f) * _spreadHeight;
        }

        public void SetLookAtWeight(float value)
        {
            _lookAtWeight = value;
        }

        private void Update()
        {
            var moveInput = transform.InverseTransformDirection(_navAgent.velocity);

            var isSimpleRot = true;
            _charMover.SetInputs(moveInput, _targetPos, _lookAtWeight, isSimpleRot);

            _charAttacker.SetInputs(_isAttack);
            _isAttack = false;
        }
    }
}

