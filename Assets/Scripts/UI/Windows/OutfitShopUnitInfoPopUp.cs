﻿using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class OutfitShopUnitInfoPopUp : AbstractPopUp
    {
        public static OutfitShopUnitInfoPopUp Instance = null;

        [SerializeField]
        private TextMeshProUGUI _displayNameTextArea = null;
        [SerializeField]
        private TextMeshProUGUI _descriptionTextArea = null;

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void Show(string displayName, string description)
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            _displayNameTextArea.text = displayName;
            _descriptionTextArea.text = description;

            base.Show();
        }

        public void Hide()
        {
            base.Hide();
        }
    }
}