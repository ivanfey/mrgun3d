﻿using Effects;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Character
{
    public interface IDamageable
    {
        void Damage(float damage, GameObject sender, RaycastHit hit);
        bool IsDie();
    }

    [RequireComponent(typeof(Animator))]
    public class CharacterStats : MonoBehaviour, IDamageable
    {
        [System.Serializable]
        public class Main
        {
            public float MaxHealth = 100f;
            public float Health = 100f;

            public float GetInhHealth()
            {
                return Health / MaxHealth;
            }
        }

        [System.Serializable]
        public class Move
        {
            public float AnimSens = 7f;
            public float TargetMoveSens = 5f;
            public float RotationLuft = 60f;
            public float MoveSpeed = 2f;
        }

        public Main MainStats = new Main();
        public Move MoveStats = new Move();
        
        public UnityEvent OnDamaged = new UnityEvent();
        public UnityEvent OnDie = new UnityEvent();

        [SerializeField]
        private string _impactEffectName = "Blood";
        [SerializeField]
        private AudioSource _impactAudioSource = null;

        public void Damage(float damage, GameObject sender, RaycastHit hit)
        {
            _impactAudioSource.PlayOneShot(_impactAudioSource.clip);

            if (Mathf.RoundToInt(MainStats.Health) <= 0)
                return;

            EffectSpawner.SpawnEffect(hit.point, hit.normal, _impactEffectName);

            MainStats.Health -= Mathf.RoundToInt(damage);

            OnDamaged.Invoke();

            if (Mathf.RoundToInt(MainStats.Health) <= 0)
                OnDie.Invoke();
        }

        public bool IsDie()
        {
            return MainStats.Health <= 0;
        }
    }
}

