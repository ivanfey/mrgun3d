﻿using Core;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Windows
{
    public class MainMenuWindow : AbstractWindow
    {
        [SerializeField]
        private Button _startButton = null;
        [SerializeField]
        private Button _shopButton = null;

        public override void Show(UnityAction onShowed = null)
        {
            Analytics.AnalyticsController.OpenWindow(gameObject.name);

            base.Show(onShowed);

        }

        private void OnEnable()
        {
            _startButton.onClick.AddListener(SetStart);
            _shopButton.onClick.AddListener(ShowShop);
        }

        private void OnDisable()
        {
            _startButton.onClick.RemoveListener(SetStart);
            _shopButton.onClick.RemoveListener(ShowShop);
        }

        private void SetStart()
        {
            MainController.IsStart = true;
        }

        private void ShowShop()
        {
            WindowController.Instance.SetNewWindow("Shop");
        }
    }
}