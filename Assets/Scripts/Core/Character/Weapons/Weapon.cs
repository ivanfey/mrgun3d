﻿using System.Collections;
using UnityEngine;

namespace Core.Character.Weapons
{
    public class Weapon : MonoBehaviour
    {
        protected Animator _anim = null;
        protected CharacterAttacker _attacker = null;

        protected void Init()
        {
            _anim = GetComponentInParent<Animator>();

            _attacker = GetComponentInParent<CharacterAttacker>();
        }
    }
}