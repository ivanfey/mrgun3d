﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class BlackListRewardUnit : MonoBehaviour
    {
        public enum GiftType
        {
            Money25,
            Money50,
            Money75,
            Money100,
            Weapon
        }

        [SerializeField]
        private GiftType _giftType = GiftType.Money25;
        [SerializeField]
        private GameObject _giftGO = null;
        [SerializeField]
        private GameObject _doneGO = null;
        [SerializeField]
        private GameObject _selectedGO = null;
        [SerializeField]
        private TextMeshProUGUI _giftName = null;

        public void SetGiftName(string newName)
        {
            if(_giftName != null)
                _giftName.text = newName;
        }

        public void SetSelector(bool state)
        {
            _selectedGO.SetActive(state);
        }

        public void SetStart()
        {
            _giftGO.SetActive(true);
            _selectedGO.SetActive(false);
            _doneGO.SetActive(false);
        }

        public void SetDone()
        {
            _giftGO.SetActive(false);
            _selectedGO.SetActive(false);
            _doneGO.SetActive(true);
        }

        public bool GetIsDone()
        {
            return _doneGO.activeSelf;
        }

        public GiftType GetGiftType()
        {
            return _giftType;
        }
    }
}