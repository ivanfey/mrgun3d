﻿using TMPro;
using UnityEngine;

namespace UI.Windows
{
    public class WeaponShopUnitInfoPopUp : AbstractPopUp
    {
        public static WeaponShopUnitInfoPopUp Instance = null;

        [SerializeField]
        private TextMeshProUGUI _displayNameTextArea = null;
        [SerializeField]
        private TextMeshProUGUI _descriptionTextArea = null;
        [SerializeField]
        private ShopUnit.CustomDataParams[] _customDataParams = new ShopUnit.CustomDataParams[0];

        private void OnEnable()
        {
            Instance = this;
        }

        private void OnDisable()
        {
            Instance = null;
        }

        public void Show(string displayName, string description, ShopUnit.CustomDataParams[] customDataParams)
        {
            Analytics.AnalyticsController.OpenPopUp(gameObject.name);

            _displayNameTextArea.text = displayName;
            _descriptionTextArea.text = description;

            for (int i = 0; i < _customDataParams.Length; i++)
            {
                for (int j = 0; j < customDataParams.Length; j++)
                {
                    if (customDataParams[j].ParamName != _customDataParams[i].ParamName)
                        continue;

                    _customDataParams[i].ParamTextArea.text = customDataParams[j].ParamValue;
                    break;
                }
            }

            base.Show();
        }

        public void Hide()
        {
            base.Hide();
        }
    }
}

