﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UI.Windows
{
    public abstract class AbstractWindow : MonoBehaviour
    {
        [SerializeField]
        private string _name = "";

        [SerializeField]
        private Transform _root = null;
        [SerializeField]
        private Transform _addedRoot = null;

        [SerializeField]
        protected float _showTime = 0.75f;
        [SerializeField]
        protected AnimationCurve _showCurve = new AnimationCurve();
        [SerializeField]
        protected Transform _showPoint = null;
        [SerializeField]
        protected Transform _addedShowPoint = null;

        [SerializeField]
        protected float _hideTime = 0.75f;
        [SerializeField]
        protected AnimationCurve _hideCurve = new AnimationCurve();
        [SerializeField]
        protected Transform _hidePoint = null;
        [SerializeField]
        protected Transform _addedHidePoint = null;

        public bool Check(string windowName)
        {
            return _name == windowName;
        }

        public virtual void Init()
        {

        }

        public virtual void Show(UnityAction onShowed = null)
        {
            if (_moving != null)
                StopCoroutine(_moving);

            gameObject.SetActive(true);
            _moving = Moving(_showTime, _showPoint, _addedShowPoint, _showCurve, ()=> 
            {
                onShowed?.Invoke();
            });
            StartCoroutine(_moving);
        }

        public virtual void Hide(UnityAction onHided = null)
        {
            if (_moving != null)
                StopCoroutine(_moving);

            _moving = Moving(_hideTime, _hidePoint, _addedHidePoint, _hideCurve, () => 
            {
                gameObject.SetActive(false);
                onHided?.Invoke();
            });
            StartCoroutine(_moving);
        }

        private IEnumerator _moving = null;
        private IEnumerator Moving(float moveTime, Transform endPoint, Transform addedEndPoint, AnimationCurve moveCurve, UnityAction onDoned)
        {
            var startPos = _root.position;
            var addedStartPos = _addedRoot == null ? Vector3.zero : _addedRoot.position;

            var timer = 0f;
            while (timer < moveTime)
            {
                yield return null;
                timer += Time.deltaTime;

                _root.position = Vector3.Lerp(startPos, endPoint.position, moveCurve.Evaluate(timer/moveTime));
                if(_addedRoot != null)
                    _addedRoot.position = Vector3.Lerp(addedStartPos, addedEndPoint.position, moveCurve.Evaluate(timer / moveTime));
            }

            _moving = null;

            onDoned?.Invoke();
        }
    }
}