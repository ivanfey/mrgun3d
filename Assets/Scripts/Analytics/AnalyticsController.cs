﻿using Core;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

namespace Analytics
{
    public class AnalyticsController : MonoBehaviour
    {
        private static bool _isFireBaseInited = false;
        private static bool _isFaceBookInited = false;

        private void OnEnable()
        {
            Firebase.Messaging.FirebaseMessaging.TokenReceived += OnFirebaseTokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived += OnFirebaseMessageReceived;
        }

        private void OnDisable()
        {
            Firebase.Messaging.FirebaseMessaging.TokenReceived -= OnFirebaseTokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived -= OnFirebaseMessageReceived;
        }

        private void Start()
        {
            _isFireBaseInited = false;
            _isFaceBookInited = false;
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => 
            {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    _isFireBaseInited = true;
                    Debug.Log("FirebaseInitDone");
                }
                else
                {
                    Debug.LogError(System.String.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                }
            });

            FB.Init(() => 
            {
                Debug.Log("Facebook init done");
                _isFaceBookInited = true;
            });
        }

        public void OnFirebaseTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
        {
            Debug.Log("Received Registration Token: " + token.Token);
        }

        public void OnFirebaseMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
        {
            Debug.Log("Received a new message from: " + e.Message.From);
        }

        private static List<Firebase.Analytics.Parameter> GetDefaultParamsFirebase()
        {
            var playerIdParam = new Firebase.Analytics.Parameter("PlayerId", Core.Web.LoginController.LoginRes != null ? Core.Web.LoginController.LoginRes.PlayFabId : "none");
            var playerNameParam = new Firebase.Analytics.Parameter("PlayerName", Core.Web.LoginController.UserName != null ? Core.Web.LoginController.UserName : "none");
            var playerLevelParam = new Firebase.Analytics.Parameter("PlayerLevel", Core.Web.InventoryController.Level);
            var playerMoneyParam = new Firebase.Analytics.Parameter("PlayerMoney", Core.Web.InventoryController.Money);
            var playerWeaponIdParam = new Firebase.Analytics.Parameter("PlayerWeaponId", PlayerPrefs.GetString(MainController.WeaponInHandsSave, MainController.FirstWeaponInHandId));
            var playerOutfitIdParam = new Firebase.Analytics.Parameter("PlayerOutfitId", PlayerPrefs.GetString(MainController.OutfitOnCharacterSave, MainController.FirstOutfitOnCharacterId));

            var tempParams = new List<Firebase.Analytics.Parameter>();
            tempParams.Add(playerIdParam);
            tempParams.Add(playerNameParam);
            tempParams.Add(playerLevelParam);
            tempParams.Add(playerMoneyParam);
            tempParams.Add(playerWeaponIdParam);
            tempParams.Add(playerOutfitIdParam);

            return tempParams;
        }

        private static Dictionary<string, object> GetDefaultParamsFacebook()
        {
            var tempParams = new Dictionary<string, object>();
            tempParams.Add("PlayerId", Core.Web.LoginController.LoginRes != null ? Core.Web.LoginController.LoginRes.PlayFabId : "none");
            tempParams.Add("PlayerName", Core.Web.LoginController.UserName != null ? Core.Web.LoginController.UserName : "none");
            tempParams.Add("PlayerLevel", Core.Web.InventoryController.Level);
            tempParams.Add("PlayerMoney", Core.Web.InventoryController.Money);
            tempParams.Add("PlayerWeaponId", PlayerPrefs.GetString(MainController.WeaponInHandsSave, MainController.FirstWeaponInHandId));
            tempParams.Add("PlayerOutfitId", PlayerPrefs.GetString(MainController.OutfitOnCharacterSave, MainController.FirstOutfitOnCharacterId));

            return tempParams;
        }

        public static void OpenWindow(string windowName)
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                facebookTempParams.Add("WindowName", windowName);
                FB.LogAppEvent("OpenWindow", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var openedWindowNameParam = new Firebase.Analytics.Parameter("WindowName", windowName);
            var tempParams = GetDefaultParamsFirebase();
            tempParams.Add(openedWindowNameParam);
            Firebase.Analytics.FirebaseAnalytics.LogEvent("OpenWindow", tempParams.ToArray());
        }

        public static void OpenPopUp(string popUpName)
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                facebookTempParams.Add("PopUpName", popUpName);
                FB.LogAppEvent("OpenPopUp", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var openedPopUpNameParam = new Firebase.Analytics.Parameter("PopUpName", popUpName);
            var tempParams = GetDefaultParamsFirebase();
            tempParams.Add(openedPopUpNameParam);
            Firebase.Analytics.FirebaseAnalytics.LogEvent("OpenPopUp", tempParams.ToArray());
        }

        public static void BuyWeapon(string weaponId)
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                facebookTempParams.Add("NewWeaponId", weaponId);
                FB.LogAppEvent("BuyWeapon", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var newWeaponIdParam = new Firebase.Analytics.Parameter("NewWeaponId", weaponId);
            var tempParams = GetDefaultParamsFirebase();
            tempParams.Add(newWeaponIdParam);
            Firebase.Analytics.FirebaseAnalytics.LogEvent("BuyWeapon", tempParams.ToArray());
        }

        public static void BuyOutfit(string outfitId)
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                facebookTempParams.Add("NewOutfitId", outfitId);
                FB.LogAppEvent("BuyOutfit", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var newOutfitIdParam = new Firebase.Analytics.Parameter("NewOutfitId", outfitId);
            var tempParams = GetDefaultParamsFirebase();
            tempParams.Add(newOutfitIdParam);
            Firebase.Analytics.FirebaseAnalytics.LogEvent("BuyOutfit", tempParams.ToArray());
        }

        public static void Die()
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                FB.LogAppEvent("Die", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var tempParams = GetDefaultParamsFirebase();
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Die", tempParams.ToArray());
        }

        public static void KillEnemy()
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                FB.LogAppEvent("KillEnemy", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var tempParams = GetDefaultParamsFirebase();
            Firebase.Analytics.FirebaseAnalytics.LogEvent("KillEnemy", tempParams.ToArray());
        }

        public static void KillBoss()
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                FB.LogAppEvent("KillBoss", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var tempParams = GetDefaultParamsFirebase();
            Firebase.Analytics.FirebaseAnalytics.LogEvent("KillBoss", tempParams.ToArray());
        }

        public static void WatchAdsForSecondLife()
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                FB.LogAppEvent("WatchAdsForSecondLife", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var tempParams = GetDefaultParamsFirebase();
            Firebase.Analytics.FirebaseAnalytics.LogEvent("WatchAdsForSecondLife", tempParams.ToArray());
        }

        public static void WatchAdsForSecondSpin()
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                FB.LogAppEvent("WatchAdsForSecondSpin", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var tempParams = GetDefaultParamsFirebase();
            Firebase.Analytics.FirebaseAnalytics.LogEvent("WatchAdsForSecondSpin", tempParams.ToArray());
        }

        public static void WatchAdsForMoney()
        {
            if (_isFaceBookInited)
            {
                var facebookTempParams = GetDefaultParamsFacebook();
                FB.LogAppEvent("WatchAdsForMoney", parameters: facebookTempParams);
            }

            if (!_isFireBaseInited)
                return;
            var tempParams = GetDefaultParamsFirebase();
            Firebase.Analytics.FirebaseAnalytics.LogEvent("WatchAdsForMoney", tempParams.ToArray());
        }
    }
}

