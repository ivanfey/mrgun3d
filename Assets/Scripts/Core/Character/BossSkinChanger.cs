﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Character
{
    public class BossSkinChanger : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _maleSkins = new GameObject[0];
        [SerializeField]
        private GameObject[] _femaleSkins = new GameObject[0];

        public void SetRandomSkin(bool isMale, int skinId)
        {
            var randomValue = new System.Random(skinId);

            if (isMale)
            {
                for (int i = 0; i < _femaleSkins.Length; i++)
                {
                    _femaleSkins[i].SetActive(false);
                }
                var randomSkin = randomValue.Next(0, _maleSkins.Length);
                for (int i = 0; i < _maleSkins.Length; i++)
                {
                    _maleSkins[i].SetActive(i == randomSkin);
                }
            }
            else
            {
                for (int i = 0; i < _maleSkins.Length; i++)
                {
                    _maleSkins[i].SetActive(false);
                }
                var randomSkin = randomValue.Next(0, _femaleSkins.Length);
                for (int i = 0; i < _femaleSkins.Length; i++)
                {
                    _femaleSkins[i].SetActive(i == randomSkin);
                }
            }
        }
    }
}