﻿using UnityEngine;

namespace Core.Character.Weapons
{
    public class WeaponGetter : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _weaponPrefs = new GameObject[0];

        public void SetWeapon(Transform parent)
        {
            Instantiate(_weaponPrefs[Random.Range(0, _weaponPrefs.Length)], parent);
        }
    }
}